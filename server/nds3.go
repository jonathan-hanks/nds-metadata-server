//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package server

import (
	"encoding/json"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/nds2parser"
	"io"
	"log"
	"math"
	"net"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"
)

import (
	"git.ligo.org/nds/nds-metadata-server/mds"
)

type dbFunc func() *mds.MetaDataStore

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	index := strings.Index(p[1:], "/")
	if index >= 0 {
		return p[1 : index+1], p[index+1:]
	}
	return p[1:], ""
}

type allChannels struct{}

func (a *allChannels) Matches(channel *common.BasicChannel) bool {
	return true
}

type paramChannelMatcher struct {
	Class            common.ClassType
	Type             common.DataType
	MinRate, MaxRate float64
}

func (m *paramChannelMatcher) Matches(channel *common.BasicChannel) bool {
	if !m.Class.CompatibleWith(channel.Class) ||
		!m.Type.CompatibleWith(channel.Type) ||
		m.MinRate > channel.Rate || m.MaxRate <= channel.Rate {
		return false
	}
	return true
}

type patternChannelMatcher struct {
	Pattern nds2parser.BashPattern
}

func (m *patternChannelMatcher) Matches(channel *common.BasicChannel) bool {
	return m.Pattern.Pattern.MatchString(channel.Name)
}

type fullChannelMatcher struct {
	paramChannelMatcher
	patternChannelMatcher
}

func (m *fullChannelMatcher) Matches(channel *common.BasicChannel) bool {
	return m.paramChannelMatcher.Matches(channel) && m.patternChannelMatcher.Matches(channel)
}

type StatusCapture struct {
	w          http.ResponseWriter
	statusCode int
}

func NewStatusCapture(w http.ResponseWriter) StatusCapture {
	return StatusCapture{w: w}
}

func (l *StatusCapture) Header() http.Header {
	return l.w.Header()
}

func (l *StatusCapture) Write(data []byte) (int, error) {
	if l.statusCode == 0 {
		l.statusCode = http.StatusOK
	}
	return l.w.Write(data)
}

func (l *StatusCapture) WriteHeader(statusCode int) {
	l.w.WriteHeader(statusCode)
	l.statusCode = statusCode
}

func (l *StatusCapture) StatusCode() int {
	return l.statusCode
}

func LogRequest(handler func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		statusCapture := NewStatusCapture(w)
		handler(&statusCapture, req)
		log.Printf("%s %s %s %d", req.Method, req.URL.Path, req.URL.Query(), statusCapture.StatusCode())
	}
}

func getChannelPredicateFromQuery(queryParams url.Values, fallback mds.ChannelPredicate) (mds.ChannelPredicate, common.TimeSpan, string) {

	prefix := ""
	timeSpan := common.CreateTimeSpan(0, common.GpsInf)
	pred := fallback
	if queryParams != nil {

		gpsStart := common.GpsSecond(0)
		gpsEnd := common.GpsInf
		pattern := nds2parser.BashPattern{}
		reqClass := common.CreateClassType(common.ClassTypeUnknown)
		reqType := common.CreateDataType(common.DataTypeUnknown)
		minRate := 0.0
		maxRate := math.MaxFloat64

		paramPred := false
		usePattern := false

		if val, ok := queryParams["class"]; ok && len(val) > 0 {
			reqClass.SetString(val[0])
			paramPred = true
		}
		if val, ok := queryParams["type"]; ok && len(val) > 0 {
			reqType.SetString(val[0])
			paramPred = true
		}
		if val, ok := queryParams["min_rate"]; ok && len(val) > 0 {
			if rate, err := strconv.ParseFloat(val[0], 64); err == nil && rate >= 0.0 {
				minRate = rate
				paramPred = true
			}
		}
		if val, ok := queryParams["max_rate"]; ok && len(val) > 0 {
			if rate, err := strconv.ParseFloat(val[0], 64); err == nil && rate >= 0.0 {
				maxRate = rate
				paramPred = true
			}
		}
		if maxRate < minRate {
			tmp := maxRate
			maxRate = minRate
			minRate = tmp
		}
		if val, ok := queryParams["pattern"]; ok && len(val) > 0 && val[0] != "*" {
			var err error
			pattern, err = nds2parser.ParseBashPattern(val[0])
			if err == nil {
				usePattern = true
			}
		}

		if val, ok := queryParams["start_gps"]; ok && len(val) > 0 {
			if gps, err := strconv.ParseInt(val[0], 10, 64); err == nil && gps >= 0 {
				gpsStart = gps
			}
		}
		if val, ok := queryParams["end_gps"]; ok && len(val) > 0 {
			if gps, err := strconv.ParseInt(val[0], 10, 64); err == nil && gps >= 0 {
				gpsEnd = gps
			}
		}
		if gpsEnd < gpsStart {
			tmp := gpsEnd
			gpsEnd = gpsStart
			gpsStart = tmp
		}
		timeSpan = common.CreateTimeSpan(gpsStart, gpsEnd)

		if paramPred || usePattern {
			var predMatcher *paramChannelMatcher
			var patternMatcher *patternChannelMatcher
			if paramPred {
				predMatcher = &paramChannelMatcher{
					Class:   reqClass,
					Type:    reqType,
					MinRate: minRate,
					MaxRate: maxRate,
				}
				pred = predMatcher
			}
			if usePattern {
				patternMatcher = &patternChannelMatcher{Pattern: pattern}
				pred = patternMatcher
				prefix = patternMatcher.Pattern.CommonPrefix
			}
			if paramPred && usePattern {
				pred = &fullChannelMatcher{
					paramChannelMatcher:   *predMatcher,
					patternChannelMatcher: *patternMatcher,
				}
			}
		}
	}
	return pred, timeSpan, prefix
}

func channelHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	pred := mds.ChannelPredicate(&allChannels{})
	pred, timeSpan, prefix := getChannelPredicateFromQuery(req.URL.Query(), pred)
	useJson := false
	encoder := json.NewEncoder(w)
	if query := req.URL.Query(); query != nil {
		if query.Get("format") == "json" {
			useJson = true
		}
	}

	subset := db.GetChannelView(prefix)
	subset.ApplyToChannelsIf(func(curChan *mds.ChannelAndAvailability) {
		if useJson {
			encoder.Encode(curChan)
		} else {
			w.Write([]byte(fmt.Sprintf("%s %f %s %s\n",
				curChan.Channel.Name, curChan.Channel.Rate,
				curChan.Channel.Class.String(),
				curChan.Channel.Type.String())))
		}
	}, pred, timeSpan)
}

func replicationHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	if path == "" {
		w.Header().Set("Content-Type", "application/json")
		var replicationReq mds.SummaryRequest
		if req.Method == "POST" {
			defer req.Body.Close()

			decoder := json.NewDecoder(req.Body)
			if err := decoder.Decode(&replicationReq); err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

		}

		_ = db.ReplicationStream(w, replicationReq)
		return

	}
	command, tail := shiftPath(path)
	if tail != "" {
		http.NotFound(w, req)
		return
	}
	switch command {
	case "summary":
		w.Header().Set("Content-Type", "application/json")
		summary := db.GetReplicationSummary()
		encoder := json.NewEncoder(w)
		_ = encoder.Encode(&summary)
	default:
		http.NotFound(w, req)
		return
	}
}

func frameIntervalsHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	frameType, tail := shiftPath(path)
	starts, tail := shiftPath(tail)
	ends, tail := shiftPath(tail)
	if tail != "" || frameType == "" || starts == "" || ends == "" {
		http.NotFound(w, req)
		return
	}
	start, err1 := strconv.ParseInt(starts, 10, 64)
	end, err2 := strconv.ParseInt(ends, 10, 64)
	if err1 != nil || err2 != nil {
		http.NotFound(w, req)
		return
	}
	intervals := db.FrameFileIntervals(frameType, common.CreateTimeSpan(start, end))
	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	for _, span := range intervals {
		if err := encoder.Encode(&span); err != nil {
			return
		}
	}
}

func framePathHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	frameType, tail := shiftPath(path)
	starts, tail := shiftPath(tail)
	ends, tail := shiftPath(tail)
	if frameType == "" || starts == "" || ends == "" || tail != "" {
		http.NotFound(w, req)
		return
	}
	start, err1 := strconv.ParseInt(starts, 10, 64)
	end, err2 := strconv.ParseInt(ends, 10, 64)
	if err1 != nil || err2 != nil {
		http.NotFound(w, req)
		return
	}

	intervals := db.FrameFilePaths(frameType, common.CreateTimeSpan(start, end))
	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	for _, entry := range intervals {
		if err := encoder.Encode(&entry); err != nil {
			return
		}
	}
}

func frameDetailHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	frameType, tail := shiftPath(path)
	starts, tail := shiftPath(tail)
	ends, tail := shiftPath(tail)
	if frameType == "" || starts == "" || ends == "" || tail != "" {
		http.NotFound(w, req)
		return
	}
	start, err1 := strconv.ParseInt(starts, 10, 64)
	end, err2 := strconv.ParseInt(ends, 10, 64)
	if err1 != nil || err2 != nil {
		http.NotFound(w, req)
		return
	}

	frameView := db.FrameView()
	epoch := common.CreateTimeSpan(start, end)
	frameIndex := frameView.FrameIndex(frameType)
	spans := frameView.FrameExistance(frameIndex)

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	for _, entry := range spans {
		curEntry := entry.ToCommon()
		if common.TimeSpansOverlap(epoch, curEntry) {
			curEntry = common.ConstrainTimeSpans(curEntry, epoch)
			if err := encoder.Encode(&curEntry); err != nil {
				return
			}
		}
	}
}

func framesHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request) {
	head, tail := shiftPath(path)
	switch head {
	case "intervals":
		frameIntervalsHandler(db, tail, w, req)
	case "paths":
		framePathHandler(db, tail, w, req)
	case "detailed":
		frameDetailHandler(db, tail, w, req)
	default:
		http.NotFound(w, req)
		return
	}
}

func remoteProxyHandler(db *mds.MetaDataStore, frameType string, w http.ResponseWriter, channels []string, start, end int64) {
	parts := strings.SplitN(frameType, ")", 2)
	if len(parts) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	label := parts[0][1:]
	proxy, err := db.GetRemoteProxy(label)
	if err != nil {
		log.Printf("Unable to find remote proxy for remote server with label %s", label)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	reader, err := proxy.ProxyDataStream(parts[1], channels, common.CreateTimeSpan(start, end))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer reader.Close()
	_, _ = io.Copy(w, reader)
}

func localProxyHandler(db *mds.MetaDataStore, frameType string, w http.ResponseWriter, channels []string, start, end int64, readerAddr *net.TCPAddr) {
	if readerAddr == nil {
		log.Println("local data proxy requested, but not configured")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("This server is not configured to handle remote data requests"))
		return
	}
	conn, err := net.DialTCP("tcp", nil, readerAddr)
	if err != nil {
		log.Printf("Unable to connecto to local reader at %v", readerAddr)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Unable to connect to local reader"))
		return
	}
	defer conn.Close()
	channelList := ""
	for i, ch := range channels {
		if i > 0 {
			channelList += " "
		}
		channelList += ch
	}
	cmd := fmt.Sprintf("__get-frame-data %s %d %d {%s};\nquit;\n", frameType, start, end, channelList)
	if _, err := conn.Write([]byte(cmd)); err != nil {
		log.Printf("Unable to send command to reader")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Error sending command to reader"))
		return
	}
	var daqStatus [4]byte
	if _, err := io.ReadFull(conn, daqStatus[:]); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Unable to read response from the reader"))
		return
	}
	if string(daqStatus[:]) != "0000" {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("error on read"))
		return
	}
	_, _ = io.Copy(w, conn)
}

func proxyHandler(db *mds.MetaDataStore, path string, w http.ResponseWriter, req *http.Request, readerAddr *net.TCPAddr) {
	if req.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		_, _ = w.Write([]byte("Only POST allowed"))
		return
	}
	head, tail := shiftPath(path)
	if head != "data" {
		http.NotFound(w, req)
		return
	}
	frameType, tail := shiftPath(tail)
	starts, tail := shiftPath(tail)
	ends, tail := shiftPath(tail)
	if frameType == "" || starts == "" || ends == "" || tail != "" {
		http.NotFound(w, req)
		return
	}
	start, err1 := strconv.ParseInt(starts, 10, 64)
	end, err2 := strconv.ParseInt(ends, 10, 64)
	if err1 != nil || err2 != nil {
		http.NotFound(w, req)
		return
	}
	//if req.Header.Get("ContentType") != "application/json" {
	//	w.WriteHeader(http.StatusBadRequest)
	log.Printf("proxy request not application/json, found %s instead", req.Header.Get("ContentType"))
	//	_, _ = w.Write([]byte("Content-type must be json"))
	//	return
	//}
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	channels := make([]string, 0, 10)
	for decoder.More() {
		var channel string
		if err := decoder.Decode(&channel); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			log.Printf("unable to decode proxy channel list")
			_, _ = w.Write([]byte("Unable to parse request"))
			return
		}
		channels = append(channels, channel)
	}
	if strings.HasPrefix(frameType, "(") {
		remoteProxyHandler(db, frameType, w, channels, start, end)
	} else {
		localProxyHandler(db, frameType, w, channels, start, end, readerAddr)
	}
}

func indexHandler(db *mds.MetaDataStore, w http.ResponseWriter, req *http.Request, readerAddr *net.TCPAddr) {
	head, tail := shiftPath(req.URL.Path)

	req.Header.Set("Cache-Control", "no-store")
	switch head {
	case "channels":
		channelHandler(db, tail, w, req)
		return
	case "replicate":
		replicationHandler(db, tail, w, req)
		return
	case "frames":
		framesHandler(db, tail, w, req)
		return
	case "proxy":
		proxyHandler(db, tail, w, req, readerAddr)
		return
	}
	_, _ = w.Write([]byte(fmt.Sprintln("Welcome to an experimental nds3 interface.\nDo *NOT* plan on using this.")))
	_, _ = w.Write([]byte(fmt.Sprintf("Your request was '%s' '%s'\n", req.Method, req.URL.Path)))
	_, _ = w.Write([]byte(fmt.Sprintf("Your arguments where %v", req.URL.Query())))
}

func ServeNds3(listener net.Listener, db func() *mds.MetaDataStore, localReaderAddr string) {
	var readerAddr *net.TCPAddr
	if localReaderAddr != "" {
		addr, err := net.ResolveTCPAddr("tcp", localReaderAddr)
		if err != nil {
			log.Printf("Unable to proxy data for remote servers, address lookup failed for the local reader, %v", err)
		}
		readerAddr = addr
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", LogRequest(func(w http.ResponseWriter, req *http.Request) {
		indexHandler(db(), w, req, readerAddr)
	}))
	server := http.Server{Handler: mux}
	log.Printf("Serving nds3 experimental from %s", listener.Addr().String())
	err := server.Serve(listener)
	log.Printf("Nds3 experimental server exited with error %v", err)
}
