//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package frame_indexer

// //#cgo pkg-config: framecpp
// #cgo CXXFLAGS: -std=c++11
// #cgo LDFLAGS: -lframecpp  -lframecpp8 -lframecpp7 -lframecpp6 -lframecpp4 -lframecpp3 -lframecppcmn -lldastoolsal
// #include <stdlib.h>
// #include "frame_indexer.h"
//
//
// indexed_channel* _index_frame(_GoString_ s, int *count) {
//   return index_frame(_GoStringPtr(s), _GoStringLen(s), count);
// }
import "C"

import (
	"math"
	"unsafe"
)

import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

func IndexFrame(path string) []common.BasicChannel {
	const bigIndex = math.MaxInt32 - 1
	var count C.int
	data := C._index_frame(path, &count)
	defer C.free_index_channel_list(data)

	channelCount := int64(count)
	if channelCount < 0 || channelCount > int64(bigIndex) {
		return make([]common.BasicChannel, 0, 0)
	}
	channels := make([]common.BasicChannel, channelCount, channelCount)

	var coreChans []C.indexed_channel
	// See https://groups.google.com/forum/#!topic/golang-nuts/sV_f0VkjZTA
	// for mapping into the C memory array
	coreChans = (*[bigIndex]C.indexed_channel)(unsafe.Pointer(data))[0:channelCount]
	for i := int64(0); i < channelCount; i++ {
		channels[i] = common.BasicChannel{
			Name:  C.GoString(&coreChans[i].name[0]),
			Type:  common.CreateDataType(byte(coreChans[i].data_type)),
			Class: common.CreateClassType(byte(coreChans[i].class_type)),
			Rate:  float64(coreChans[i].rate),
		}
	}

	return channels
}
