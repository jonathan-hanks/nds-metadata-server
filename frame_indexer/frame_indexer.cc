//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//
#include "frame_indexer.h"
#include <stdlib.h>
#include <cstring>
#include <string>
#include <iostream>
#include <unordered_set>

#include "framecpp/FrameH.hh"
#include "framecpp/FrVect.hh"
// #include "framecpp/Dimension.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/IFrameStream.hh"
#include "ldastoolsal/fstream.hh"

int fcpp_to_data_type(int fcpp) {
    switch (fcpp) {
        case FrameCPP::FrVect::FR_VECT_2S: return FR_DATA_TYPE_Int16;
        case FrameCPP::FrVect::FR_VECT_4S: return FR_DATA_TYPE_Int32;
        case FrameCPP::FrVect::FR_VECT_8S: return FR_DATA_TYPE_Int64;
        case FrameCPP::FrVect::FR_VECT_4R: return FR_DATA_TYPE_Float32;
        case FrameCPP::FrVect::FR_VECT_8R: return FR_DATA_TYPE_Float64;
        case FrameCPP::FrVect::FR_VECT_8C: return FR_DATA_TYPE_Complex32;
        case FrameCPP::FrVect::FR_VECT_4U: return FR_DATA_TYPE_Uint32;
        default:
            return FR_DATA_TYPE_Unknown;
    }
}

indexed_channel *
index_frame(const std::string& path, int* channel_count)
{
    auto trend_types = std::unordered_set<std::string>{".n", ".min", ".max", ".mean", ".rms"};
    FrameCPP::IFrameFStream f_stream(path.c_str());
    auto frame = f_stream.ReadNextFrame();
    auto raw = frame->GetRawData();
    auto& proc_ref = frame->RefProcData();

    if (!channel_count)
    {
        return nullptr;
    }
    *channel_count = 0;

    int raw_size = 0;
    int reduced_size = proc_ref.size();
    if (raw) {
        raw_size = (*raw).RefFirstAdc().size();
    }
    auto dest_size = raw_size + reduced_size;
    std::unique_ptr<indexed_channel[]> dest{ new indexed_channel[dest_size] };
    auto dest_end = dest.get() + dest_size;
    auto dest_cur = dest.get();

    if (raw)
    {
        for (const auto &adc:raw->RefFirstAdc())
        {
            auto &name = adc->GetName();
            auto rate = adc->GetSampleRate();
            auto class_type = FR_CLASS_TYPE_Raw;
            auto& data = adc->RefData().front();
            auto data_type = fcpp_to_data_type(data->GetType());

            auto index = name.find_last_of('.');
            if (index != std::string::npos)
            {
                auto ext = name.substr(index);
                if (trend_types.find(ext) != trend_types.end())
                {
                    if (rate == 1.0)
                    {
                        class_type = FR_CLASS_TYPE_STrend;
                    }
                    else
                    {
                        class_type = FR_CLASS_TYPE_MTrend;
                    }
                }
            }
            if (name.size() > INDEX_CHANNEL_NAME_LEN-1) {
                std::cout << "Skipping " << name << "\n";
                continue;
            }
            if (dest_cur < dest_end)
            {
                std::strcpy(dest_cur->name, name.c_str());
                dest_cur->rate = rate;
                dest_cur->data_type = data_type;
                dest_cur->class_type = class_type;
                ++dest_cur;
                ++(*channel_count);
            }
            else
            {
                throw std::runtime_error("Unexpected channel");
            }
        }
    }
    for (const auto& proc:proc_ref)
    {
        auto& name = proc->GetName();
        auto class_type = FR_CLASS_TYPE_Reduced;
        auto& data = proc->RefData().front();
        auto rate = 1.0/data->GetDim(0).GetDx();
        auto data_type = fcpp_to_data_type(data->GetType());

        if (dest_cur < dest_end)
        {
            std::strcpy(dest_cur->name, name.c_str());
            dest_cur->rate = rate;
            dest_cur->data_type = data_type;
            dest_cur->class_type = class_type;
            ++dest_cur;
            ++(*channel_count);
        }
        else
        {
            throw std::runtime_error("Unexpected channel");
        }
    }
    return dest.release();
}

extern "C" {

const int FR_DATA_TYPE_Unknown = 0xff;
const int FR_DATA_TYPE_Int16 = 0x1;
const int FR_DATA_TYPE_Int32 = 0x2;
const int FR_DATA_TYPE_Int64 = 0x4;
const int FR_DATA_TYPE_Float32 = 0x8;
const int FR_DATA_TYPE_Float64 = 0x10;
const int FR_DATA_TYPE_Complex32 = 0x20;
const int FR_DATA_TYPE_Uint32 = 0x40;

const int FR_CLASS_TYPE_Unknown = 0xff;
const int FR_CLASS_TYPE_Online = 0x01;
const int FR_CLASS_TYPE_Raw = 0x02;
const int FR_CLASS_TYPE_Reduced = 0x04;
const int FR_CLASS_TYPE_STrend = 0x08;
const int FR_CLASS_TYPE_MTrend = 0x10;
const int FR_CLASS_TYPE_TestPt = 0x20;
const int FR_CLASS_TYPE_Static = 0x40;

void
free_index_channel_list(indexed_channel* channels)
try
{
    if (channels)
    {
        std::unique_ptr<indexed_channel[]> p{channels};
    }
}
catch(...)
{

}


indexed_channel* index_frame(const char* path, size_t path_len, int *channel_count)
try
{
    return index_frame(std::string(path, path_len), channel_count);
}
catch(...)
{
    return nullptr;
}
}