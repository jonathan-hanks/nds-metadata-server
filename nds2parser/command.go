//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

const (
	authorizeLiteral        = "authorize"
	protocolVersionLiteral  = "server-protocol-version"
	protocolRevisionLiteral = "server-protocol-revision"
	quiteLiteral            = "quit"
	listEpochsLiteral       = "list-epochs"
	setEpochsLiteral        = "set-epoch"
	countChannelsLiteral    = "count-channels"
	getChannelsLiteral      = "get-channels"
	getChannelCrcLiteral    = "get-channel-crc"
	getSourceListLiteral    = "get-source-list"
	getSourceDataLiteral    = "get-source-data"
)

type commandState struct {
	Data      string
	Cmd       Command
	Name      string
	GpsTime   common.GpsSecond
	ClassType common.ClassType
	DataType  common.DataType
	Epoch     common.TimeSpan
	Pattern   BashPattern
	Float     float64
	Channels  []common.BasicChannel
	Results   *ParsedCommand
}

func (s *commandState) Save() string {
	return s.Data
}

func (s *commandState) Restore(input string) {
	s.Data = input
}

func (s *commandState) RestoreIfFailed(input string, pred *bool) {
	if !*pred {
		s.Data = input
	}
}

func (s *commandState) Str() string {
	return s.Data
}

func (s *commandState) SetStr(input string) {
	s.Data = input
}

func createCommandState(input string) *commandState {
	return &commandState{Data: input,
		Cmd:       CreateCommand(CommandUnknown),
		ClassType: common.CreateClassType(common.ClassTypeUnknown),
		DataType:  common.CreateDataType(common.DataTypeUnknown),
		Channels:  make([]common.BasicChannel, 0, 0),
		Epoch:     common.CreateTimeSpan(0, common.GpsInf),
		Results: &ParsedCommand{Command: CreateCommand(CommandUnknown),
			Version:  1,
			Revision: 5}}
}

func (s *commandState) CommandParsed(cmd int) {
	s.Cmd.Set(cmd)
}

func (s *commandState) RevisionSpecifierParsed(revision int) {
	s.Results.Revision = revision
}

func (s *commandState) PosIntParsed(time common.GpsSecond) {
	s.GpsTime = time
}

func (s *commandState) ClassTypeParsed(classType byte) {
	s.ClassType.Set(classType)
}

func (s *commandState) DataTypeParsed(dataType byte) {
	s.DataType.Set(dataType)
}

func (s *commandState) PatternParsed(pattern BashPattern) {
	s.Pattern = pattern
}

func (c *commandState) ChannelNameParsed(name string) {
	c.Name = name
}

func (s *commandState) ChannelParsed(channel common.BasicChannel) {
	s.Channels = append(s.Channels, channel)
}

func (s *commandState) FloatParsed(val float64) {
	s.Float = val
}

func (s *commandState) EpochParsed(val common.TimeSpan) {
	s.Results.TimeSpan = val
}

func ParseCommand(input string) *ParsedCommand {
	state := createCommandState(input)
	if parseCommand(state) {
		state.Results.Command = state.Cmd
	}
	return state.Results
}

// Parse a nds2 command
func parseCommand(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)

	state.Data = trimBlanks(state.Data)
	if parseNop(state) {
		result = true
		return
	}
	commandSelection := func() bool {
		if parseAuthorize(state) {
			return true
		} else if parseProtocolVersion(state) {
			return true
		} else if parseProtocolRevision(state) {
			return true
		} else if parseQuit(state) {
			return true
		} else if parseListEpochs(state) {
			return true
		} else if parseSetEpoch(state) {
			return true
		} else if parseCountChannels(state) {
			return true
		} else if parseGetChannels(state) {
			return true
		} else if parseGetChannelCrc(state) {
			return true
		} else if parseGetSourceList(state) {
			return true
		} else if parseGetSourceData(state) {
			return true
		}
		return false
	}

	if commandSelection() {
		state.Data = trimBlanks(state.Data)
		if len(state.Data) == 0 {
			result = true
			return
		}
	}

	result = false
	return
}

func parseNop(state *commandState) bool {
	if len(state.Data) == 0 {
		state.CommandParsed(CommandNop)
		return true
	}
	return false
}

func parseAuthorize(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false

	if state.Data, ok = consumeIfMatches(state.Data, authorizeLiteral); !ok {
		return
	}
	state.CommandParsed(CommandAuthorize)
	result = true
	return
}

func parseProtocolVersion(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false
	if state.Data, ok = consumeIfMatches(state.Data, protocolVersionLiteral); !ok {
		return
	}

	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}

	state.CommandParsed(CommandProtocolVersion)
	result = true
	return
}

func parseProtocolRevision(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false
	if state.Data, ok = consumeIfMatches(state.Data, protocolRevisionLiteral); !ok {
		return
	}

	_ = parseProtocolRevisionSpecifier(state)

	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}

	state.CommandParsed(CommandProtocolRevision)
	result = true
	return
}

func parseProtocolRevisionSpecifier(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false

	if !parseSpace(state) {
		return
	}
	if state.Data, ok = consumeIfMatches(state.Data, "6"); ok {
		state.RevisionSpecifierParsed(6)
		return true
	} else if state.Data, ok = consumeIfMatches(state.Data, "7"); ok {
		state.RevisionSpecifierParsed(7)
		return true
	}
	return false
}

func parseQuit(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false
	if state.Data, ok = consumeIfMatches(state.Data, quiteLiteral); !ok {
		return
	}
	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}
	state.CommandParsed(CommandQuit)
	result = true
	return
}

func parseListEpochs(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false

	if state.Data, ok = consumeIfMatches(state.Data, listEpochsLiteral); !ok {
		return
	}
	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}
	state.CommandParsed(CommandListEpochs)
	result = true
	return
}

func parseSetEpoch(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false

	if state.Data, ok = consumeIfMatches(state.Data, setEpochsLiteral); !ok {
		return
	}
	if !parseSpace(state) {
		return
	}
	if !parsePosInt(state) {
		return
	}
	start := state.GpsTime
	if state.Data, ok = consumeIfMatches(state.Data, "-"); !ok {
		return
	}
	if !parsePosInt(state) {
		return
	}
	end := state.GpsTime

	if end < start {
		return
	}

	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}

	state.EpochParsed(common.CreateTimeSpan(start, end))
	state.CommandParsed(CommandSetEpoch)
	result = true
	return
}

func doParseCountGetChannels(state *commandState, literal string, commandVal int) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	var ok bool
	result = false

	if state.Data, ok = consumeIfMatches(state.Data, literal); !ok {
		return
	}
	if !parseSpace(state) {
		return
	}

	if !parsePosInt(state) {
		return
	}

	state.Results.GpsTime = state.GpsTime

	if !parseSpace(state) {
		return
	}

	if !parseClassType(state) {
		return
	}

	state.Results.ClassType = state.ClassType

	if parseSpace(state) {
		if !parseOpenBrace(state) || !parseChannelPattern(state) || !parseCloseBrace(state) {
			return
		}
		state.Results.Pattern = state.Pattern
	}

	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}
	state.CommandParsed(commandVal)
	result = true
	return
}

func parseCountChannels(state *commandState) bool {
	return doParseCountGetChannels(state, countChannelsLiteral, CommandCountChannels)
}

func parseGetChannels(state *commandState) bool {
	return doParseCountGetChannels(state, getChannelsLiteral, CommandGetChannels)
}

func parseGetChannelCrc(state *commandState) bool {
	return doParseCountGetChannels(state, getChannelCrcLiteral, CommandGetChannelCrc)
}

func doParseGetSourceListData(state *commandState, literal string, commandVal int) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false
	var ok bool

	if state.Data, ok = consumeIfMatches(state.Data, literal); !ok {
		return
	}
	if !parseSpace(state) {
		return
	}

	if !parsePosInt(state) {
		return
	}

	state.Results.GpsTime = state.GpsTime

	if !parseSpace(state) {
		return
	}

	if !parseChannelList(state) {
		return
	}

	if state.Data, ok = consumeIfMatches(state.Data, semicolonLiteral); !ok {
		return
	}
	state.Results.Channels = state.Channels
	state.CommandParsed(commandVal)
	result = true
	return
}

func parseGetSourceList(state *commandState) bool {
	return doParseGetSourceListData(state, getSourceListLiteral, CommandGetSourceList)
}

func parseGetSourceData(state *commandState) bool {
	return doParseGetSourceListData(state, getSourceDataLiteral, CommandGetSourceData)
}

func parseChannelList(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	if !parseOpenBrace(state) {
		return
	}

	if parseChannelSpecifier(state) {
		for {
			if !parseSpace(state) {
				break
			}
			if !parseChannelSpecifier(state) {
				return
			}
		}
	}
	if !parseCloseBrace(state) {
		return
	}
	result = true
	return
}

func parseChannelSpecifier(state *commandState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	if !parseChannelName(state) {
		return
	}

	name := state.Name
	rate := 0.0
	classType := common.CreateClassType(common.ClassTypeUnknown)
	dataType := common.CreateDataType(common.DataTypeUnknown)

	for {
		if !parseChannelPartSep(state) {
			break
		}
		if parseFloat(state) {
			rate = state.Float
			continue
		}
		if parseClassType(state) {
			if state.ClassType.Type == common.ClassTypeUnknown {
				return
			}
			classType = state.ClassType
			continue
		}
		if parseDataType(state) {
			if state.DataType.Type == common.DataTypeUnknown {
				return
			}
			dataType = state.DataType
			continue
		}
		return
	}

	state.ChannelParsed(common.BasicChannel{
		Name:  name,
		Type:  dataType,
		Class: classType,
		Rate:  rate,
	})
	result = true
	return
}

func parseChannelPartSep(state *commandState) bool {
	var ok bool
	if state.Data, ok = consumeIfMatches(state.Data, ","); ok {
		return true
	} else if state.Data, ok = consumeIfMatches(state.Data, "%"); ok {
		return true
	}
	return false
}
