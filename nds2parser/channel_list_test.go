//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"git.ligo.org/nds/nds-metadata-server/common"
	"math"
	"testing"
)

func AboutEqual(a, b, tolerance float64) bool {
	return math.Abs(a-b) < tolerance
}

func checkChannel(t *testing.T, expected, actual common.BasicChannel) {
	if expected.Name != actual.Name {
		t.Logf("Unexpected name, expected '%s', got '%s'", expected.Name, actual.Name)
		t.Fail()
	}
	if expected.Class != actual.Class {
		t.Logf("Unexpected class type, expected '%d', got '%d'", expected.Class.Type, actual.Class.Type)
		t.Fail()
	}
	if expected.Type != actual.Type {
		t.Logf("Unexpected date type, expected '%d', got '%d'", expected.Type.Type, actual.Type.Type)
		t.Fail()
	}
	if !AboutEqual(expected.Rate, actual.Rate, 0.0001) {
		t.Logf("Unexpected rate, expected '%f', got '%f'", expected.Rate, actual.Rate)
		t.Fail()
	}
}

func checkSpans(t *testing.T, expected, actual []SourceSpan) {
	if len(expected) != len(actual) {
		t.Fatalf("Differing span lengths, expected '%d' got '%d'", len(expected), len(actual))
	}
	for i, entry := range actual {
		if expected[i].FrameType != entry.FrameType {
			t.Logf("Wrong frame span type, expected '%s', got '%s'", expected[i].FrameType, entry.FrameType)
			t.Fail()
		}
		if expected[i].Start != entry.Start {
			t.Logf("Wrong span start time, expected '%d', got '%d'", expected[i].Start, entry.Start)
			t.Fail()
		}
		if expected[i].End != entry.End {
			t.Logf("Wrong span end time, expected '%d', got '%d'", expected[i].End, entry.End)
			t.Fail()
		}
	}
}

func TestChannelList(t *testing.T) {
	chanList, err := ChannelListLine("H1:SYS-SOME_CHANNEL_NAME1.mean m-trend 0 real_8 0.0166667 H-H1_T 1000000000:1000 H-H1_T 1100000000:899999999")
	if err != nil {
		t.Fatalf("Unexpected error parsing input, %v", err)
	}
	checkChannel(t, common.BasicChannel{Name: "H1:SYS-SOME_CHANNEL_NAME1.mean",
		Class: common.CreateClassType(common.ClassTypeMTrend),
		Type:  common.CreateDataType(common.DataTypeFloat64),
		Rate:  1 / 60.0}, chanList.BasicChannel)
	checkSpans(t, []SourceSpan{
		{FrameType: "H-H1_T", Start: 1000000000, End: 1000000000 + 1000},
		{FrameType: "H-H1_T", Start: 1100000000, End: 1100000000 + 899999999},
	}, chanList.Spans)
}

func TestChannelListWithNoAvailability(t *testing.T) {
	chanList, err := ChannelListLine("H1:SYS-SOME_CHANNEL_NAME1.mean m-trend 0 real_8 0.0166667 H-H1_M")
	if err != nil {
		t.Fatalf("Unexpected error parsing input, %v", err)
	}
	checkChannel(t, common.BasicChannel{Name: "H1:SYS-SOME_CHANNEL_NAME1.mean",
		Class: common.CreateClassType(common.ClassTypeMTrend),
		Type:  common.CreateDataType(common.DataTypeFloat64),
		Rate:  1 / 60.0}, chanList.BasicChannel)
	checkSpans(t, []SourceSpan{
		{FrameType: "H-H1_M", Start: 0, End: 0},
	}, chanList.Spans)
}

func TestChannelListWithTildeInName(t *testing.T) {
	chanList, err := ChannelListLine("H1:SYS-SOME_CHANNEL_NAME~1.mean m-trend 0 real_8 0.0166667 H-H1_T 1000000000:1000 H-H1_T 1100000000:899999999")
	if err != nil {
		t.Fatalf("Unexpected error parsing input, %v", err)
	}
	checkChannel(t, common.BasicChannel{Name: "H1:SYS-SOME_CHANNEL_NAME~1.mean",
		Class: common.CreateClassType(common.ClassTypeMTrend),
		Type:  common.CreateDataType(common.DataTypeFloat64),
		Rate:  1 / 60.0}, chanList.BasicChannel)
	checkSpans(t, []SourceSpan{
		{FrameType: "H-H1_T", Start: 1000000000, End: 1000000000 + 1000},
		{FrameType: "H-H1_T", Start: 1100000000, End: 1100000000 + 899999999},
	}, chanList.Spans)
}

func TestChannelListWithSlashInName(t *testing.T) {
	chanList, err := ChannelListLine("H1:SYS-SOME_CHANNEL_NAME/1.mean m-trend 0 real_8 0.0166667 H-H1_T 1000000000:1000 H-H1_T 1100000000:899999999")
	if err != nil {
		t.Fatalf("Unexpected error parsing input, %v", err)
	}
	checkChannel(t, common.BasicChannel{Name: "H1:SYS-SOME_CHANNEL_NAME/1.mean",
		Class: common.CreateClassType(common.ClassTypeMTrend),
		Type:  common.CreateDataType(common.DataTypeFloat64),
		Rate:  1 / 60.0}, chanList.BasicChannel)
	checkSpans(t, []SourceSpan{
		{FrameType: "H-H1_T", Start: 1000000000, End: 1000000000 + 1000},
		{FrameType: "H-H1_T", Start: 1100000000, End: 1100000000 + 899999999},
	}, chanList.Spans)
}
