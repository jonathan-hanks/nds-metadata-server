//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"errors"
	"regexp"
	"strings"
	"unicode/utf8"
)

type BashPattern struct {
	RePattern    string
	Pattern      *regexp.Regexp
	CommonPrefix string
}

type bashPatternBuilder struct {
	PatternBuilder strings.Builder
	PrefixBuilder  strings.Builder
	CurDepth       int

	// Set to true when we can no longer extract literals from
	// the initial prefix
	NoMoreLiterals bool

	InitialPrefix string
	// True when we are working on a common prefix in the first
	// set of choices, ie 'X1:{PEM-A|PEM-B|PEM-C}...'
	// We want to extract a common prefix of 'X1:PEM-' which means
	// we must go into the first level of choices
	BuildingSecondaryPrefix bool
	NoMoreSecondaries       bool
	LongestSecondaryPrefix  string
}

func (b *bashPatternBuilder) GetPrefix() string {
	return b.InitialPrefix + b.LongestSecondaryPrefix
}

func (b *bashPatternBuilder) Pattern() BashPattern {
	pattern := "^" + b.PatternBuilder.String() + "$"
	compiled, _ := regexp.Compile(pattern)
	return BashPattern{
		RePattern:    pattern,
		Pattern:      compiled,
		CommonPrefix: b.PrefixBuilder.String(),
	}
}

func (b *bashPatternBuilder) incrementDepth() {
	b.CurDepth++
}

func (b *bashPatternBuilder) decrementDepth() {
	b.CurDepth--
}

func (b *bashPatternBuilder) AddRuneToPattern(ch rune) {
	const escapedChars = "^$[]|()+.*?"
	if strings.ContainsRune(escapedChars, ch) {
		b.PatternBuilder.WriteRune('\\')

	}
	b.PatternBuilder.WriteRune(ch)
	if b.CurDepth == 1 && !b.NoMoreLiterals {
		b.PrefixBuilder.WriteRune(ch)
	}
}

func (b *bashPatternBuilder) AddEscapedRuneToPattern(ch rune) {
	b.PatternBuilder.WriteRune(ch)
	if b.CurDepth == 1 && !b.NoMoreLiterals {
		b.PrefixBuilder.WriteRune(ch)
	}
}

func (b *bashPatternBuilder) AddStringToPattern(input string) {
	escapeNext := false
	for _, ch := range input {
		if escapeNext {
			b.AddRuneToPattern(ch)
			escapeNext = false
			continue
		}
		if ch == '\\' {
			escapeNext = true
			continue
		}
		switch ch {
		case '*':
			b.PatternBuilder.WriteString(".*")
			b.NoMoreLiterals = true
		case '?':
			b.PatternBuilder.WriteRune('.')
			b.NoMoreLiterals = true
		case '{':
			b.PatternBuilder.WriteRune('(')
		case '}':
			b.PatternBuilder.WriteRune(')')
			b.NoMoreLiterals = true
		case ',':
			if b.CurDepth <= 1 {
				b.PatternBuilder.WriteRune(',')
				b.PrefixBuilder.WriteRune(',')
			} else {
				b.PatternBuilder.WriteRune('|')
			}
		default:
			b.AddRuneToPattern(ch)
		}
	}
}

type bashPatternState struct {
	Data    string
	Pattern BashPattern
}

func (s *bashPatternState) Save() string {
	return s.Data
}

func (s *bashPatternState) Restore(input string) {
	s.Data = input
}

func (s *bashPatternState) RestoreIfFailed(input string, pred *bool) {
	if !*pred {
		s.Data = input
	}
}

func (s *bashPatternState) Str() string {
	return s.Data
}

func (s *bashPatternState) SetStr(val string) {
	s.Data = val
}

func (s *bashPatternState) PatternParsed(p BashPattern) {
	s.Pattern = p
}

var patternError error

func init() {
	patternError = errors.New("Unable to parse channel pattern")
}

func ParseBashPattern(input string) (BashPattern, error) {
	s := &bashPatternState{Data: input}
	if !parseChannelPattern(s) {
		return BashPattern{}, patternError
	}
	return s.Pattern, nil
}

func parseChannelPattern(state patternParser) bool {
	b := &bashPatternBuilder{}
	result := doParseChannelPattern(state, b)
	if result {
		pattern := b.Pattern()
		compiled, err := regexp.Compile(pattern.RePattern)
		if err != nil {
			return false
		}
		pattern.Pattern = compiled
		state.PatternParsed(b.Pattern())
	} else {
		return false
	}
	return result
}

func doParseChannelPattern(state patternParser, b *bashPatternBuilder) (result bool) {
	result = false
	if b.CurDepth > 100 {
		return
	}
	defer state.RestoreIfFailed(state.Save(), &result)
	b.incrementDepth()
	defer b.decrementDepth()

	const allowedSingles = digits + alpha + ":-_.*?"
	const escapePrefix = "\\"

	if b.CurDepth > 1 {
		b.AddStringToPattern("{")
	}

	for {
		progress := false
		trimmed := strings.TrimLeft(state.Str(), allowedSingles)
		if len(trimmed) != len(state.Str()) {
			data := state.Str()
			b.AddStringToPattern(data[:len(data)-len(trimmed)])
			state.SetStr(trimmed)
			progress = true
		}
		trimmed, ok := consumeIfMatches(state.Str(), escapePrefix)
		if ok {
			ch, count := utf8.DecodeRuneInString(trimmed)
			if count == 0 {
				return
			}
			b.AddEscapedRuneToPattern(ch)
			trimmed = trimmed[count:]
			progress = true
		}
		state.SetStr(trimmed)
		trimmed, ok = consumeIfMatches(state.Str(), ",")
		if ok {
			if b.CurDepth == 1 {
				b.AddEscapedRuneToPattern(',')
			} else {
				b.AddStringToPattern(",")
			}
		}
		state.SetStr(trimmed)
		if parseOpenBrace(state) {
			if doParseChannelPattern(state, b) &&
				parseCloseBrace(state) {
				progress = true
			} else {
				return
			}
		}
		if !progress {
			break
		}
	}

	if b.CurDepth > 1 {
		b.AddStringToPattern("}")
	}
	result = true
	return
}
