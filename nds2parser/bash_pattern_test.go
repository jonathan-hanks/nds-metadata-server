//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"testing"
)

func TestBashPattern(t *testing.T) {
	type TestData struct {
		Input   string
		Pattern string
		Prefix  string
	}

	testCases := []TestData{
		{"?1:*PEM*", "^.1:.*PEM.*$", ""},
		{"?1:*{PEM,SUS}*{}{ABC,{D*F}}", "^.1:.*(PEM|SUS).*()(ABC|(D.*F))$", ""},
		{"?1:*{PE\\{\\}M,SUS}\\,*{}{ABC,{D*F}}", "^.1:.*(PE{}M|SUS),.*()(ABC|(D.*F))$", ""},
		{"X1:PEM*", "^X1:PEM.*$", "X1:PEM"},
		{"X?:PEM*", "^X.:PEM.*$", "X"},
		{"X1:{PEMA,PEMB}*};", "^X1:(PEMA|PEMB).*$", "X1:"},
		{"{X1:PEMA,X1:PEMB}*};", "^(X1:PEMA|X1:PEMB).*$", ""},
	}
	for _, testCase := range testCases {
		bp, err := ParseBashPattern(testCase.Input)
		if err != nil {
			t.Fail()
			t.Logf("Unable to parse pattern '%s'", testCase.Input)
		} else {
			if bp.RePattern != testCase.Pattern || bp.CommonPrefix != testCase.Prefix {
				t.Fail()
				t.Logf("Pattern '%s' expected re: '%s' prefix '%s' got '%s' '%s'",
					testCase.Input,
					testCase.Pattern,
					testCase.Prefix,
					bp.RePattern,
					bp.CommonPrefix)
			}
		}
	}
}
