#!/bin/bash

METADATA_LOGS_ROOT="nds_metadata_server.log"
IO_LOGS_ROOT="nds-io-server.log"

METADATA_LOGS=$METADATA_LOGS_ROOT
IO_LOGS_ROOT=$IO_LOGS_ROOT

NDS2_LISTEN=localhost
NDS2_PORT=31200
METADATA_LISTEN=localhost:31201
NDS_WEB_LISTEN=localhost:31202

METADATA_PID=0

FRAME_DIR="./frames"
FRAME_DB="./frames/frame_index.json.gz"

function kill_proc {
    if [ $1 -gt 0 ]; then
        echo "Closing process $1"
        kill $1
    fi
}

function cleanup {
    kill_proc $METADATA_PID
}

function usage {
  echo "Run a nds2 server using a local metadata server and a local reader."
  echo "Options:"
  echo "    -L <log path>         Directory to write logs to [./]"
  echo "    -l <host>             Interface for the nds2 server [localhost]"
  echo "    -p <port>             Port for the nds2 server [31200]"
  echo "    -m <host:port>        Interface:port for the metadata server [localhost:31201]"
  echo "    -f <dir>              Directory containing frames to index [./frames]"
  echo "    -d <file name>        File name of the frame index [./frames/frame_index.json.gz]"
  echo ""
}

while getopts "L:l:p:m:f:d:" OPTION; do
  case $OPTION in
    L)
      if [[ ! -d "$OPTARG" ]]; then
        echo "The option to -L must be a directory"
        exit 1
      fi
      METADATA_LOGS="$OPTARG/$METADATA_LOGS_ROOT"
      IO_LOGS="$OPTARG/$IO_LOGS_ROOT"
      ;;
    l)
      NDS2_LISTEN="$OPTARG"
      ;;
    p)
      NDS2_PORT="$OPTARG"
      ;;
    m)
      METADATA_LISTEN="$OPTARG"
      ;;
    f)
      FRAME_DIR="$OPTARG"
      ;;
    d)
      FRAME_DB="$OPTARG"
      ;;
    *)
      usage
      exit 1
      ;;
  esac
done

trap cleanup EXIT

echo "Starting the metadata server $METADATA_PID"
echo nds_metadata_server --frame-dir "$FRAME_DIR" --json-db "$FRAME_DB" --listen "$METADATA_LISTEN"
nds_metadata_server --frame-dir "$FRAME_DIR" --json-db "$FRAME_DB" --listen "$METADATA_LISTEN" &> "$METADATA_LOGS" &
METADATA_PID=$!

echo "Starting the nds2 interface at $NDS2_LISTEN:$NDS2_PORT"
echo nds2-io-node -l $NDS2_LISTEN -p $NDS2_PORT -i nds2://$METADATA_LISTEN -f nds3://$METADATA_LISTEN
nds2-io-node -l $NDS2_LISTEN -p $NDS2_PORT -i nds2://$METADATA_LISTEN -f nds3://$METADATA_LISTEN &> "$IO_LOGS"

