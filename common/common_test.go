//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package common

import (
	"encoding/json"
	"testing"
)

func TestDataTypeSetString(t *testing.T) {
	var testCases = []struct {
		Value    string
		Expected byte
	}{
		{"int_2", DataTypeInt16},
		{"int_4", DataTypeInt32},
		{"int_8", DataTypeInt64},
		{"real_4", DataTypeFloat32},
		{"real_8", DataTypeFloat64},
		{"complex_8", DataTypeComplex32},
		{"uint_4", DataTypeUint32},
		{"unknown", DataTypeUnknown},
		{"int", DataTypeUnknown},
		{"", DataTypeUnknown},
		{"float", DataTypeUnknown},
	}

	for _, testCase := range testCases {
		var dt DataType
		dt.SetString(testCase.Value)
		if dt.Type != testCase.Expected {
			t.Fail()
			t.Logf("'%s' resolved to %d instead of %d", testCase.Value, dt.Type, testCase.Expected)
		}
		if testCase.Expected != DataTypeUnknown {
			if dt.String() != testCase.Value {
				t.Fail()
				t.Logf("cross check failed for '%s' got '%s'", testCase.Value, dt.String())
			}
		}
	}
}

func TestDataTypeMarshalJSON(t *testing.T) {
	testCases := []byte{
		DataTypeUnknown, DataTypeInt16, DataTypeInt32, DataTypeInt64, DataTypeFloat32, DataTypeFloat64, DataTypeComplex32, DataTypeUint32,
	}
	for _, test := range testCases {
		initial := CreateDataType(test)
		data, err := json.Marshal(&initial)
		if err != nil {
			t.Fail()
			t.Logf("Error in marshalling, %v", err)
			continue
		}
		final := DataType{}
		err = json.Unmarshal(data, &final)
		if err != nil {
			t.Fail()
			t.Logf("Unable to unmarshall value, %v", err)
		}
		if initial.Type != final.Type {
			t.Fail()
			t.Logf("Expected type value of %d got %d", int(initial.Type), int(final.Type))
		}
	}
}

func TestClassTypeMarshalJSON(t *testing.T) {
	testCases := []byte{
		ClassTypeUnknown, ClassTypeRaw, ClassTypeReduced, ClassTypeSTrend, ClassTypeMTrend, ClassTypeStatic, ClassTypeTestPt, ClassTypeOnline,
	}
	for _, test := range testCases {
		initial := CreateClassType(test)
		data, err := json.Marshal(&initial)
		if err != nil {
			t.Fail()
			t.Logf("Error in marshalling, %v", err)
			continue
		}
		final := ClassType{}
		err = json.Unmarshal(data, &final)
		if err != nil {
			t.Fail()
			t.Logf("Unable to unmarshall value, %v", err)
		}
		if initial.Type != final.Type {
			t.Fail()
			t.Logf("Expected type value of %d got %d", int(initial.Type), int(final.Type))
		}
	}
}

func TestClassTypeSetString(t *testing.T) {
	var testCases = []struct {
		Value    string
		Expected byte
	}{
		{"online", ClassTypeOnline},
		{"raw", ClassTypeRaw},
		{"reduced", ClassTypeReduced},
		{"s-trend", ClassTypeSTrend},
		{"m-trend", ClassTypeMTrend},
		{"test-pt", ClassTypeTestPt},
		{"static", ClassTypeStatic},
		{"unknown", ClassTypeUnknown},
		{"unexpected", ClassTypeUnknown},
		{"", ClassTypeUnknown},
		{"float", ClassTypeUnknown},
	}

	for _, testCase := range testCases {
		var ct ClassType
		ct.SetString(testCase.Value)
		if ct.Type != testCase.Expected {
			t.Fail()
			t.Logf("'%s' resolved to %d instead of %d", testCase.Value, ct.Type, testCase.Expected)
		}
		if testCase.Expected != DataTypeUnknown {
			if ct.String() != testCase.Value {
				t.Fail()
				t.Logf("cross check failed for '%s' got '%s'", testCase.Value, ct.String())
			}
		}
	}
}

func TestTimeSpanMarshallJson(t *testing.T) {
	ts := CreateTimeSpan(1000000000, 1200000000)
	data, err := json.Marshal(&ts)
	if err != nil {
		t.Fail()
		t.Logf("Unable to marshal timespan, %v", err)
		return
	}
	final := TimeSpan{}
	err = json.Unmarshal(data, &final)
	if err != nil {
		t.Fail()
		t.Logf("Unable to unmarshal timespan, %v", err)
		return
	}
	if final.Start() != ts.Start() || final.End() != ts.End() {
		t.Fail()
		t.Logf("Expected (%d, %d) got (%d, %d)", ts.Start(), ts.End(), final.Start(), final.End())
	}
}

func TestTimeSpansOverlap(t *testing.T) {
	var testCases = []struct {
		t1, t2   TimeSpan
		expected bool
	}{
		{CreateTimeSpan(0, GpsInf), CreateTimeSpan(1000000000, 1000000001), true},
		{CreateTimeSpan(1000000000, 1000000001), CreateTimeSpan(0, GpsInf), true},
		{CreateTimeSpan(1000000000, 1000000001), CreateTimeSpan(1000000001, GpsInf), false},
		{CreateTimeSpan(1000000001, GpsInf), CreateTimeSpan(1000000000, 1000000001), false},
		{CreateTimeSpan(1000000000, 1000000001), CreateTimeSpan(1000000000, 1000000001), true},
		{CreateTimeSpan(1000000000, 1000000005), CreateTimeSpan(1000000003, 1000000006), true},
		{CreateTimeSpan(1000000003, 1000000006), CreateTimeSpan(1000000000, 1000000005), true},
	}
	for _, testCase := range testCases {
		actual := TimeSpansOverlap(testCase.t1, testCase.t2)
		if actual != testCase.expected {
			t.Logf("[%d, %d) [%d, %d) Expected %v got %v",
				testCase.t1.Start(), testCase.t1.End(), testCase.t2.Start(), testCase.t2.End(), testCase.expected, actual)
			t.Fail()
		}
	}
}

func TestCombineTimeSpans(t *testing.T) {
	var testCases = []struct {
		t1, t2, expected TimeSpan
	}{
		{CreateTimeSpan(-5, 10), CreateTimeSpan(0, 14), CreateTimeSpan(-5, 14)},
		{CreateTimeSpan(0, 10), CreateTimeSpan(-5, 14), CreateTimeSpan(-5, 14)},
		{CreateTimeSpan(0, 14), CreateTimeSpan(-5, 10), CreateTimeSpan(-5, 14)},
		{CreateTimeSpan(-5, -4), CreateTimeSpan(5, 14), CreateTimeSpan(-5, 14)},
		{CreateTimeSpan(-3, 5), CreateTimeSpan(6, 12), CreateTimeSpan(-3, 12)},
	}
	for _, testCase := range testCases {
		actual := CombineTimeSpans(testCase.t1, testCase.t2)
		if actual != testCase.expected {
			t.Logf("Combining %s and %s, Expected %s got %s",
				TimeSpanAsRangeString(testCase.t1), TimeSpanAsRangeString(testCase.t2), TimeSpanAsRangeString(testCase.expected), TimeSpanAsRangeString(actual))
			t.Fail()
		}
	}
}

func TestCostrainTimeSpans(t *testing.T) {
	var testCases = []struct {
		t1, t2, expected TimeSpan
	}{
		{CreateTimeSpan(-5, 10), CreateTimeSpan(0, 14), CreateTimeSpan(0, 10)},
		{CreateTimeSpan(0, 10), CreateTimeSpan(-5, 14), CreateTimeSpan(0, 10)},
		{CreateTimeSpan(0, 14), CreateTimeSpan(-5, 10), CreateTimeSpan(0, 10)},
	}
	for _, testCase := range testCases {
		actual := ConstrainTimeSpans(testCase.t1, testCase.t2)
		if actual != testCase.expected {
			t.Logf("Constraining %s and %s, Expected %s got %s",
				TimeSpanAsRangeString(testCase.t1), TimeSpanAsRangeString(testCase.t2), TimeSpanAsRangeString(testCase.expected), TimeSpanAsRangeString(actual))
			t.Fail()
		}
	}
}
