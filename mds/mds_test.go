//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"encoding/json"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/data_test"
	"testing"
)

func TestTimeSpan_Contains(t *testing.T) {
	type testData struct {
		start, end, value common.GpsSecond
		expected          bool
	}
	testCases := []testData{
		{1155473408, 1155477504, 1155473407, false},
		{1155473408, 1155477504, 1155473408, true},
		{1155473408, 1155477504, 1155473409, true},
		{1155473408, 1155477504, 1155477503, true},
		{1155473408, 1155477504, 1155477504, false},
	}
	for _, testCase := range testCases {
		ts := CreateTimeSpan(testCase.start, testCase.end)
		if ts.Contains(testCase.value) != testCase.expected {
			t.Fail()
			t.Logf("Wrong value for %d in [%d, %d) got %v", testCase.value, testCase.start, testCase.end, testCase.expected)
		}
	}
}

func TestTimeSpan_MarshallJson(t *testing.T) {
	ts := CreateTimeSpan(1000000000, 1200000000)
	data, err := json.Marshal(&ts)
	if err != nil {
		t.Fail()
		t.Logf("Unable to marshal timespan, %v", err)
		return
	}
	final := TimeSpan{}
	err = json.Unmarshal(data, &final)
	if err != nil {
		t.Fail()
		t.Logf("Unable to unmarshal timespan, %v", err)
		return
	}
	if final.Start() != ts.Start() || final.End() != ts.End() {
		t.Fail()
		t.Logf("Expected (%d, %d) got (%d, %d)", ts.Start(), ts.End(), final.Start(), final.End())
	}
}

func TestBasicAvailability_MarshalJSON(t *testing.T) {
	avail := BasicAvailability{
		Existence: TimeSpan{100000000, 1200000000},
		Availability: []AvailabilitySpan{
			{
				FrameType: 5,
				Time:      TimeSpan{100000000, 100000005},
			},
			{
				FrameType: 6,
				Time:      TimeSpan{100000015, 1200000000},
			},
		},
	}

	data, err := json.Marshal(&avail)
	if err != nil {
		t.Fail()
		t.Logf("Unable to marshal BasicAvailability, %v", err)
		return
	}
	final := BasicAvailability{Availability: make([]AvailabilitySpan, 0)}
	err = json.Unmarshal(data, &final)
	if err != nil {
		t.Fail()
		t.Logf("Unable to unmarshall BasicAvailability, %v", err)
		return
	}
	if avail.Existence != final.Existence || len(avail.Availability) != len(final.Availability) ||
		avail.Availability[0] != final.Availability[0] || avail.Availability[1] != final.Availability[1] {
		t.Fail()
		t.Logf("Could not round trip BasicAvailability")
	}
}

func TestLoadDatabaseFromReplicationStream(t *testing.T) {
	mds, err := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	if err != nil {
		t.Fail()
		t.Fatalf("Loading from replication failed with error, %v", err)
	}
	if len(mds.db) != 12 {
		t.Fatal("Wrong channel count on replication stream")
	}
	if len(mds.FrameView().frames) != 1 {
		t.Fatal("Wrong frame type count on replication stream")
	}
	if mds.localChannelChecksum == nil || len(mds.localChannelChecksum) == 0 {
		t.Fatal("The mds local channel checksum has not been calculated")
	}
}

func TestGetDBSubset(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())

	db := mds.GetChannelView("H1:GDS-CALIB_KAPPA")
	if len(db.db) != 7 {
		t.Fatalf("The db subset is wrong, got %d entries, expected %d", len(db.db), 7)
	}

	db = mds.GetChannelView("")
	if len(db.db) != len(mds.db) {
		t.Fatalf("The db subset is wrong, got %d entries, expected %d", len(db.db), 7)
	}
}

func TestMetaDataStore_FrameExistsAt(t *testing.T) {
	type TestCase struct {
		time   common.GpsSecond
		exists bool
	}
	testCases := []TestCase{
		{1000000000 - 1, false},
		{1000000000, true},
		{1000000001, true},
		{1200000000, false},
		{1300000000 - 1, false},
		{1300000000, true},
		{1300000001, true},
		{1400000000 - 1, true},
		{1400000000, false},
		{1450000000 - 1, false},
		{1450000000, true},
		{1450000001, true},
		{1500000000 - 1, true},
		{1500000000, false},
	}
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	for _, testCase := range testCases {
		actual := mds.FrameView().FrameExistsAt(0, testCase.time)
		if actual != testCase.exists {
			t.Fail()
			t.Logf("Testing for existance at %d, expected %v got %v", testCase.time, testCase.exists, actual)
		}
	}
}

func TestBasicAvailability_renumberFrames(t *testing.T) {
	testCases := []struct {
		input    BasicAvailability
		mapping  []int
		expected BasicAvailability
	}{
		{
			BasicAvailability{
				Existence: TimeSpan{0, common.GpsInf},
				Availability: []AvailabilitySpan{
					{0, TimeSpan{1000000000, 1200000000}},
					{1, TimeSpan{1000000000, 1200000000}},
					{0, TimeSpan{1000000000, 1300000000}},
					{5, TimeSpan{1000000000, 1300000000}},
				},
			},
			[]int{5, 7},
			BasicAvailability{
				Existence: TimeSpan{0, common.GpsInf},
				Availability: []AvailabilitySpan{
					{5, TimeSpan{1000000000, 1200000000}},
					{7, TimeSpan{1000000000, 1200000000}},
					{5, TimeSpan{1000000000, 1300000000}},
					{-5, TimeSpan{1000000000, 1300000000}},
				},
			},
		},
	}
	for _, testCase := range testCases {
		input := testCase.input
		input.renumberFrames(testCase.mapping)
		if input.Existence != testCase.expected.Existence {
			t.Logf("Existance does not match after remapping wanted %v got %v", testCase.expected.Existence, input.Existence)
			t.Fail()
		}
		for i, expected := range testCase.expected.Availability {
			if input.Availability[i] != expected {
				t.Fail()
				t.Logf("Availability span did not match expected value wanted %v got %v", expected, input.Availability[i])
			}
		}
	}
}
