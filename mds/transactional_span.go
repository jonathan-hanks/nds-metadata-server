//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"sync"
	"sync/atomic"
	"unsafe"
)

import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

type FrameExistence []TimeSpan

type WriteOperation func(FrameExistence) FrameExistence

type TransactionalSpan struct {
	data      unsafe.Pointer
	writeLock sync.Mutex
}

func CreateTransactionalSpan(input []common.TimeSpan) *TransactionalSpan {
	buf := make(FrameExistence, 0, len(input))
	for _, entry := range input {
		buf = append(buf, CreateTimeSpan(entry.Start(), entry.End()))
	}

	return &TransactionalSpan{
		data:      unsafe.Pointer(&buf),
		writeLock: sync.Mutex{},
	}
}

func (ts *TransactionalSpan) Read() FrameExistence {
	return *(*FrameExistence)(atomic.LoadPointer(&ts.data))
}

func (ts *TransactionalSpan) Write(updater WriteOperation) {
	ts.writeLock.Lock()
	defer ts.writeLock.Unlock()

	newData := updater(ts.Read())
	atomic.StorePointer(&ts.data, unsafe.Pointer(&newData))
}
