//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import "git.ligo.org/nds/nds-metadata-server/common"

// A view into a list of channels
//
// This is commonly used with the meta data store to view the channels or a subset of the channels.
// This acts as a read only view of a channel list.
type ChannelView struct {
	db     []ChannelAndAvailability
	source *MetaDataStore
}

func (db *ChannelView) ApplyToChannelsIf(handler ChannelHandler, pred ChannelPredicate, span common.TimeSpan) {
	if span.WideOpen() {
		for _, value := range db.db {
			if pred.Matches(&value.Channel) {
				handler(&value)
			}
		}
	} else {
		db.ApplyToChannelsIfConstrained(handler, pred, span)
	}
}

func (db *ChannelView) ApplyToChannelsIfConstrained(handler ChannelHandler, pred ChannelPredicate, epoch common.TimeSpan) {
	frameView := db.source.FrameView()
	if epoch.End() == epoch.Start()+1 {
		// looking for a specific second
		for i, _ := range db.db {
			value := &db.db[i]
			if !value.ExistsDuringGpsSecond(epoch.Start(), frameView) {
				continue
			}
			if pred.Matches(&value.Channel) {
				handler(value)
			}
		}
	} else {
		for i, _ := range db.db {
			value := &db.db[i]
			if !value.ExistsDuring(epoch) {
				continue
			}
			if pred.Matches(&value.Channel) {
				handler(value)
			}
		}
	}
}

// Given a reference channel find the best match in the channel database.
// The name must match, and any concretely specified attributes (rate, class, data type) must match.
// If the attributes are specified as masks (ie unknown) they match the attribute based on a bitwise
// check
func (db *ChannelView) ResolveChannel(reference common.BasicChannel, epoch common.TimeSpan) (*ChannelAndAvailability, bool) {
	resolver := CreateResolveChanEvaluator(reference, epoch)
	start, end := common.FindRange(len(db.db), func(index int) bool {
		return db.db[index].Channel.Name >= reference.Name
	}, func(index int) bool {
		return db.db[index].Channel.Name > reference.Name
	})
	chanRange := db.db[start:end]
	for i, _ := range chanRange {
		resolver.Process(&chanRange[i])
	}
	return resolver.Match()
}
