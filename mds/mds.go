//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"compress/gzip"
	"encoding/binary"
	"encoding/json"
	"git.ligo.org/nds/nds-metadata-server/common"
	"hash"
	"io"
	"os"
	"strconv"
	"strings"
	"sync/atomic"
)

type ChannelListHash = string
type GpsSecond = common.GpsSecond
type AUID = common.AUID

type TimeSpan struct {
	start GpsSecond
	end   GpsSecond
}

func CreateTimeSpan(start GpsSecond, end GpsSecond) TimeSpan {
	return TimeSpan{start, end}
}

func (ts *TimeSpan) Start() GpsSecond {
	return ts.start
}

func (ts *TimeSpan) End() GpsSecond {
	return atomic.LoadInt64(&ts.end)
}

func (ts *TimeSpan) Extend(new_end GpsSecond) {
	atomic.StoreInt64(&ts.end, new_end)
}

func (ts *TimeSpan) Empty() bool {
	return ts.Start() == ts.End()
}

func (ts *TimeSpan) Contains(second common.GpsSecond) bool {
	return second >= ts.Start() && second < ts.End()
}

func (ts *TimeSpan) ToCommon() common.TimeSpan {
	return common.CreateTimeSpan(ts.Start(), ts.End())
}

func (ts *TimeSpan) MarshalJSON() ([]byte, error) {
	data := [2]int64{ts.Start(), ts.End()}
	return json.Marshal(&data)
}

func (ts *TimeSpan) UnmarshalJSON(data []byte) error {
	var intermediate [2]int64
	if err := json.Unmarshal(data, &intermediate); err != nil {
		return err
	}
	ts.start = intermediate[0]
	ts.end = intermediate[1]
	return nil
}

func CombineTimeSpans(t1 TimeSpan, t2 TimeSpan) TimeSpan {

	return CreateTimeSpan(
		common.MinInt64(t1.Start(), t2.Start()),
		common.MaxInt64(t1.End(), t2.End()))
}

func TimeSpansOverlap(t1 TimeSpan, t2 TimeSpan) bool {
	return !(t1.Start() > t2.Start() || t1.End() < t2.End())
}

type BasicChannel = common.BasicChannel

type AvailabilitySpan struct {
	FrameType int      `json:"frame"`
	Time      TimeSpan `json:"t"`
}

type BasicAvailability struct {
	Existence    TimeSpan
	Availability []AvailabilitySpan
}

func (ba *BasicAvailability) MarshalJSON() ([]byte, error) {
	return json.Marshal(ba.Availability)
}

func (ba *BasicAvailability) UnmarshalJSON(data []byte) error {
	span := make([]AvailabilitySpan, 0, 10)
	if err := json.Unmarshal(data, &span); err != nil {
		return err
	}
	if len(span) == 0 {
		ba.Existence = CreateTimeSpan(0, 0)
	} else {
		ba.Existence = span[0].Time
		for _, avail := range span {
			ba.Existence = CombineTimeSpans(ba.Existence, avail.Time)
		}
	}
	ba.Availability = span
	return nil
}

func (ba *BasicAvailability) recalculateExistance() {
	existence := common.CreateTimeSpan(0, 0)
	first := true
	for _, avail := range ba.Availability {
		if first {
			existence = avail.Time.ToCommon()
			first = false
		} else {
			existence = common.CombineTimeSpans(existence, avail.Time.ToCommon())
		}
	}
	ba.Existence = CreateTimeSpan(existence.Start(), existence.End())
}

// Helper function used to change the frame references
//
// This is designed to be used by the remote server support.
// The remote servers sends channels and availaiblity with its
// frame type ordering, and it must be remapped to the local
// frame ordering.
//
// mapping is used to re-write the frame linkage.
// the new frame index = mapping[old frame index]
// if the old frame index is out of bounds for mapping the
// transform is new frame index = - old frame index
func (ba *BasicAvailability) renumberFrames(mapping []int) {
	for i, _ := range ba.Availability {
		oldIndex := ba.Availability[i].FrameType
		if oldIndex >= 0 {
			if oldIndex < len(mapping) {
				ba.Availability[i].FrameType = mapping[oldIndex]
			} else {
				ba.Availability[i].FrameType = -oldIndex
			}
		}
	}
}

// Remove any availability spans that have a negative frame index
// recalculate the existance field if needed
func (ba *BasicAvailability) removeInvalid() {
	invalid := 0
	for _, cur := range ba.Availability {
		if cur.FrameType < 0 {
			invalid++
		}
	}
	if invalid == 0 {
		return
	}
	newAvail := make([]AvailabilitySpan, len(ba.Availability)-invalid)
	destIndex := 0
	existence := common.CreateTimeSpan(0, 0)
	for _, cur := range ba.Availability {
		if cur.FrameType >= 0 {
			newAvail[destIndex] = cur
			if destIndex == 0 {
				existence = cur.Time.ToCommon()
			} else {
				existence = common.CombineTimeSpans(existence, cur.Time.ToCommon())
			}
			destIndex++
		}
	}
	ba.Existence = CreateTimeSpan(existence.Start(), existence.End())
	ba.Availability = newAvail
}

func (ba *BasicAvailability) countBannedEntries(bannedFrameTypes []int) int {
	banned := 0
	isInSequence := func(needle int, haystack []int) bool {
		for _, val := range haystack {
			if val == needle {
				return true
			}
		}
		return false
	}
	for _, cur := range ba.Availability {
		if isInSequence(cur.FrameType, bannedFrameTypes) {
			banned++
		}
	}
	return banned
}
func (ba *BasicAvailability) removeFrameTypes(bannedFrameTypes []int, dest []AvailabilitySpan) []AvailabilitySpan {
	isInSequence := func(needle int, haystack []int) bool {
		for _, val := range haystack {
			if val == needle {
				return true
			}
		}
		return false
	}

	for _, cur := range ba.Availability {
		if !isInSequence(cur.FrameType, bannedFrameTypes) {
			dest = append(dest, cur)
		}
	}
	return dest
}

func (ba *BasicAvailability) addSpan(frameType int, span TimeSpan) {
	if len(ba.Availability) > 0 {
		ba.Existence = CombineTimeSpans(ba.Existence, span)
		for i, _ := range ba.Availability {
			if ba.Availability[i].FrameType == frameType && ba.Availability[i].Time.End() == span.Start() {
				ba.Availability[i].Time.Extend(span.End())
				return
			}
		}
		ba.Availability = append(ba.Availability, AvailabilitySpan{
			FrameType: frameType,
			Time:      span,
		})
	} else {
		ba.Existence = span
		ba.Availability = []AvailabilitySpan{{
			FrameType: frameType,
			Time:      span,
		}}
	}
}

type ChannelAndAvailability struct {
	Channel BasicChannel      `json:"c"`
	Avail   BasicAvailability `json:"a"`
	Local   bool              `json:"-"`
}

func (c *ChannelAndAvailability) ExistsDuring(span common.TimeSpan) bool {
	if !common.TimeSpansOverlap(c.Avail.Existence.ToCommon(), span) {
		return false
	}
	for _, avail := range c.Avail.Availability {
		if common.TimeSpansOverlap(avail.Time.ToCommon(), span) {
			return true
		}
	}
	return false
}

func (c *ChannelAndAvailability) applyToLocalHash(h hash.Hash, frameTypes []FrameType) {
	if !c.Local {
		return
	}
	h.Write([]byte(c.Channel.Name))
	var buffer [16]byte
	buffer[0] = c.Channel.Type.Type
	buffer[1] = c.Channel.Class.Type
	h.Write(buffer[0:2])
	h.Write([]byte(strconv.FormatFloat(c.Channel.Rate, 'f', 7, 64)))
	for _, curAvail := range c.Avail.Availability {
		if len(frameTypes[curAvail.FrameType].FrameLabel) != 0 {
			continue
		}
		binary.LittleEndian.PutUint64(buffer[0:8], uint64(curAvail.Time.Start()))
		binary.LittleEndian.PutUint64(buffer[8:16], uint64(curAvail.Time.End()))
		h.Write(buffer[:])
	}
}

func (c *ChannelAndAvailability) ExistsDuringGpsSecond(second common.GpsSecond, fv *FrameView) bool {
	if !c.Avail.Existence.Contains(second) {
		return false
	}
	for _, avail := range c.Avail.Availability {
		contains := avail.Time.Contains(second)
		exists := fv.FrameExistsAt(avail.FrameType, second)
		//log.Printf("Channel %s over [%d, %d) against [%d] exists: %v contains: %v", c.Channel.Name, avail.Time.Start(), avail.Time.End(), second, exists, contains)

		//log.Print(db.FrameExistance(avail.FrameType))
		if contains && exists {
			return true
		}
	}
	return false
}

func LoadDatabaseFromChannelLists(channelLists []string, frameServer common.FrameServer) *MetaDataStore {
	fr := CreateFrameTypeRegistrar()
	for _, entry := range channelLists {
		fr.AddChannelListFile(entry)
	}
	mds := createMetaDataStore(fr, frameServer)
	return mds
}

func LoadDatabaseFromJsonDB(path string) (*MetaDataStore, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var reader io.Reader
	reader = io.Reader(f)
	if strings.HasSuffix(path, ".gz") {
		gzReader, err := gzip.NewReader(f)
		if err != nil {
			return nil, err
		}
		defer gzReader.Close()
		reader = gzReader
	}
	return LoadDatabaseFromReplicationStream(reader)
}

func LoadDatabaseFromReplicationStream(reader io.Reader) (*MetaDataStore, error) {
	jsonDb, err := readReplicationStream(reader)
	if err != nil {
		return nil, err
	}
	return createMetaDataStoreFromJsonDB(jsonDb)
}
