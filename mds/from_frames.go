//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/frame_indexer"
	"log"
	"os"
	"time"
)

type frameInfo struct {
	Name  string
	Path  string
	Start common.GpsSecond
	End   common.GpsSecond
	Type  string
	FTime time.Time
}

type jsonChannelMap struct {
	Index    map[common.BasicChannel]int
	Channels []ChannelAndAvailability
}

func createJsonChannelMap() *jsonChannelMap {
	return &jsonChannelMap{Index: make(map[common.BasicChannel]int), Channels: make([]ChannelAndAvailability, 0, 100000)}
}

func (jm *jsonChannelMap) AddChannels(channels []common.BasicChannel, frameIndex int, start, end, lastOfFrameType common.GpsSecond) {
	curTimeSpan := CreateTimeSpan(start, end)
	for i, _ := range channels {
		curChan := &channels[i]
		index, ok := jm.Index[*curChan]
		if !ok {
			index = len(jm.Channels)
			jm.Channels = append(jm.Channels, ChannelAndAvailability{
				Channel: *curChan,
			})
			jm.Channels[index].Avail.addSpan(frameIndex, curTimeSpan)
			jm.Index[*curChan] = index
		} else {
			dest := &jm.Channels[index]
			dest.Avail.addSpan(frameIndex, curTimeSpan)
		}
	}
}

func LoadDatabaseFromFrames(framePath, jsonPath string) (*MetaDataStore, error) {
	var jsonTime time.Time
	if info, err := os.Stat(jsonPath); err == nil {
		jsonTime = info.ModTime()
	}
	framesNewer := false
	frames := common.FindFramesUnderPath(framePath)
	for _, frameList := range frames {
		for _, entry := range frameList {
			if entry.FTime.After(jsonTime) {
				framesNewer = true
			}
		}
	}
	if !framesNewer {
		log.Println("Loading from jsondb instead of files as the db is newer")
		return LoadDatabaseFromJsonDB(jsonPath)
	}

	jsonDb := InternalJSONDB{Channels: make([]ChannelAndAvailability, 0, 0),
		FrameTypes:  make(map[string]int),
		Frames:      make([][]JsonDBFrameInfo, 0, 0),
		FrameSource: common.LocalFrameServerUri(framePath),
	}
	jsonChannels := createJsonChannelMap()

	for frameType, frameList := range frames {
		fmt.Println(frameType)
		frameTypeIndex := len(jsonDb.FrameTypes)
		jsonDb.FrameTypes[frameType] = frameTypeIndex
		curFrames := make([]JsonDBFrameInfo, 0, len(frameList))
		lastOfFrameType := common.GpsSecond(0)
		for _, entry := range frameList {
			if len(curFrames) == 0 || curFrames[len(curFrames)-1].TimeSpan.End() != entry.TimeSpan.Start() {
				curFrames = append(curFrames, JsonDBFrameInfo{
					FrameType: frameType,
					TimeSpan:  CreateTimeSpan(entry.TimeSpan.Start(), entry.TimeSpan.End()),
				})
			} else {
				curFrame := &curFrames[len(curFrames)-1]
				curFrame.TimeSpan.Extend(entry.TimeSpan.End())
			}
			fmt.Println("\t", entry.Name)
			jsonChannels.AddChannels(frame_indexer.IndexFrame(entry.Path), frameTypeIndex, entry.TimeSpan.Start(), entry.TimeSpan.End(), lastOfFrameType)
			lastOfFrameType = entry.TimeSpan.End()
		}
		jsonDb.Frames = append(jsonDb.Frames, curFrames)
	}

	jsonDb.Channels = jsonChannels.Channels
	mds, err := createMetaDataStoreFromJsonDB(&jsonDb)
	if err != nil {
		return nil, err
	}
	mds.saveJsonDb(jsonPath)
	return mds, nil
}
