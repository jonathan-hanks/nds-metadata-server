//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"compress/gzip"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
	"sync"
)
import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

type ChannelPredicate interface {
	Matches(channel *BasicChannel) bool
}

type ChannelHandler = func(availability *ChannelAndAvailability)

// Encapsulates the state of a remote server
type remoteDataStoreState struct {
	// remote labels and connection strings
	Remote RemoteDataStore
	// The access methods of the remote server
	RemoteInterface   RemoteInterface
	ChannelConfigHash string
}

// Describe a frame type
type FrameType struct {
	// Label, non-empty for frames from an external server
	FrameLabel string
	// base name of the frame type
	FrameName string
	// A combination of the label and frame type
	PrintName string
}

// Create a new frame type, correcty constructing the PrintName field
func NewFrameType(label, name string) FrameType {
	return FrameType{
		FrameLabel: label,
		FrameName:  name,
		PrintName:  labeledFrameType(label, name),
	}
}

func labeledFrameType(label, frameType string) string {
	longName := frameType
	if len(label) != 0 {
		longName = fmt.Sprintf("(%s)%s", label, frameType)
	}
	return longName
}

// Information used to make an optimized replication dumps.
// An unconfigured/empty struct denotes that everything should
// be dumped
// If the ChannelConfigHash is set, the channels are omitted if the
// metadata store channel config matches the given hash
// if the FrameState is specified, only updates passed those frame ranges
// are sent
type SummaryRequest struct {
	ChannelConfigHash string                      `json:"channel_config"`
	FrameState        map[string]common.GpsSecond `json:"frame_state"`
}

// Get a view onto the frame database
type FrameView struct {
	frameByName map[string]int
	nameByIndex []FrameType
	frames      []*TransactionalSpan
}

// Create a deepish copy of the FrameView
// It keeps the same transactional spans
func (fv *FrameView) Copy() FrameView {
	frameByName := make(map[string]int, len(fv.frameByName))
	nameByIndex := make([]FrameType, len(fv.nameByIndex))
	frames := make([]*TransactionalSpan, len(fv.frameByName))

	for key, val := range fv.frameByName {
		frameByName[key] = val
	}
	copy(nameByIndex, fv.nameByIndex)
	copy(frames, fv.frames)
	return FrameView{
		frameByName: frameByName,
		nameByIndex: nameByIndex,
		frames:      frames,
	}
}

func (fv *FrameView) FrameExistance(frameIndex int) []TimeSpan {
	var span []TimeSpan
	if frameIndex >= 0 && frameIndex < len(fv.frames) {
		span = fv.frames[frameIndex].Read()
	}
	return span
}

func (fv *FrameView) FrameExistsAt(frameIndex int, second common.GpsSecond) bool {
	if frameIndex < 0 || frameIndex >= len(fv.frames) {
		return false
	}
	trans := fv.frames[frameIndex].Read()
	index := sort.Search(len(trans), func(index int) bool {
		return trans[index].End() > second
	})
	if index == len(trans) {
		return false
	}
	return trans[index].Start() <= second
}

func (fv *FrameView) FrameIndex(frameType string) int {
	if val, ok := fv.frameByName[frameType]; ok {
		return val
	}
	return -1
}

func (fv *FrameView) GetFrameType(index int) (string, bool) {
	if index >= 0 && index < len(fv.nameByIndex) {
		return fv.nameByIndex[index].PrintName, true
	}
	return "", false
}
func (fv *FrameView) GetTransactionalSpan(index int) (*TransactionalSpan, string, bool) {
	if index >= 0 && index < len(fv.frames) {
		return fv.frames[index], fv.nameByIndex[index].PrintName, true
	}
	return nil, "", false
}

// to handle remote servers and combining channel lists we need a few things
// 1. a list of remote servers
// 2. Each server has a unique label that is added as a prefix to the frame type
//    any channels available through a 'marked' frame type are thus marked as
//    from an external server
type MetaDataStore struct {
	// The channel list
	db []ChannelAndAvailability

	// frame lookup information
	frames FrameView

	// frame file lookup (where it is on disk, ...)
	frameServer common.FrameServer

	localchannelCount    int
	localChannelChecksum []byte
	remoteServers        []remoteDataStoreState
	updateLock           sync.Mutex
	channelLock          sync.RWMutex
	frameLock            sync.RWMutex
}

func createNameByIndex(frameByName map[string]int) []FrameType {
	names := make([]FrameType, len(frameByName))
	for name, index := range frameByName {
		names[index] = NewFrameType("", name)
	}
	return names
}

func (mds *MetaDataStore) GetRemoteProxy(prefix string) (RemoteProxyInterface, error) {
	for _, remote := range mds.remoteServers {
		if remote.Remote.Label == prefix {
			return remote.RemoteInterface, nil
		}
	}
	return nil, errors.New("Not found")
}

func (mds *MetaDataStore) FrameView() *FrameView {
	mds.frameLock.RLock()
	defer mds.frameLock.RUnlock()
	return &mds.frames
}

func (mds *MetaDataStore) setFrameLookup(fv *FrameView) {
	mds.frameLock.Lock()
	defer mds.frameLock.Unlock()
	mds.frames = *fv
}

func (db *MetaDataStore) GetReplicationSummary() ReplicationHeader {
	lookup := db.FrameView()
	frameTypes := make([]string, 0, len(lookup.nameByIndex))
	for i, _ := range lookup.nameByIndex {
		if len(lookup.nameByIndex[i].FrameLabel) == 0 {
			frameTypes = append(frameTypes, lookup.nameByIndex[i].FrameName)
		}
	}
	return ReplicationHeader{
		NumChannels:   db.localchannelCount,
		ChannelConfig: base64.RawStdEncoding.EncodeToString(db.localChannelChecksum),
		FrameTypes:    frameTypes,
		FrameSource:   db.frameServer.ConnectionString(),
	}
}

func (db *MetaDataStore) GetChannelView(requiredPrefix string) ChannelView {
	db.channelLock.RLock()
	fullDb := db.db
	db.channelLock.RUnlock()
	prefixLen := len(requiredPrefix)
	if len(requiredPrefix) == 0 {
		return ChannelView{db: fullDb, source: db}
	}
	lowerBound := func(index int) bool {
		name := fullDb[index].Channel.Name
		curEntryLen := len(name)
		name = name[:common.MinInt(prefixLen, curEntryLen)]
		return name >= requiredPrefix
	}
	upperBound := func(index int) bool {
		name := fullDb[index].Channel.Name
		curEntryLen := len(name)
		name = name[:common.MinInt(prefixLen, curEntryLen)]
		return name > requiredPrefix
	}
	start, end := common.FindRange(len(fullDb), lowerBound, upperBound)
	return ChannelView{db: fullDb[start:end], source: db}
}

func (db *MetaDataStore) SetExternalFrameServer(server common.FrameServer) {
	db.frameServer = server
}

func (db *MetaDataStore) FrameFileIntervals(frameType string, span common.TimeSpan) []common.TimeSpan {
	if db.frameServer == nil {
		return []common.TimeSpan{}
	}
	results, _ := db.frameServer.FrameIntervals(frameType, span)
	return results
}

func (db *MetaDataStore) FrameFilePaths(frameType string, span common.TimeSpan) []string {
	if db.frameServer == nil {
		return []string{}
	}
	results, _ := db.frameServer.FrameFilePaths(frameType, span)
	return results
}

func (db *MetaDataStore) DebugDump(dest io.Writer, maxDump int) {
	view := db.GetChannelView("")
	lookup := db.FrameView()
	for i, chanAndAvail := range view.db {
		if i > maxDump {
			_, _ = fmt.Fprintln(dest, "...")
			break
		}
		_, _ = fmt.Fprintf(dest, "%s %s %s %f [%d,%d)\n",
			chanAndAvail.Channel.Name,
			chanAndAvail.Channel.Class.String(),
			chanAndAvail.Channel.Type.String(),
			chanAndAvail.Channel.Rate,
			chanAndAvail.Avail.Existence.Start(),
			chanAndAvail.Avail.Existence.End())
		for _, span := range chanAndAvail.Avail.Availability {
			_, _ = fmt.Fprintf(dest, "\t%s [%d, %d)\n", lookup.nameByIndex[span.FrameType], span.Time.Start(), span.Time.End())
		}
	}
}

func (db *MetaDataStore) ReplicationStream(dest io.Writer, summaryRequest SummaryRequest) error {
	encoder := json.NewEncoder(dest)
	encoder.SetEscapeHTML(false)
	encoder.SetIndent("", "")

	lookup := db.FrameView()
	localNames := make([]string, 0, len(lookup.nameByIndex))
	for i, _ := range lookup.nameByIndex {
		if lookup.nameByIndex[i].FrameLabel == "" {
			localNames = append(localNames, lookup.nameByIndex[i].FrameName)
		}
	}
	channelList := db.GetChannelView("").db
	omitChannels := false
	if summaryRequest.ChannelConfigHash == string(db.localChannelChecksum) {
		omitChannels = true
	}

	header := ReplicationHeader{NumChannels: db.localchannelCount,
		FrameTypes:      localNames,
		FrameSource:     db.frameServer.ConnectionString(),
		SkippedChannels: omitChannels,
	}

	if err := encoder.Encode(&header); err != nil {
		return err
	}
	if !omitChannels {
		channelCount := 0
		for i, _ := range channelList {
			if !channelList[i].Local {
				continue
			}
			if channelCount > db.localchannelCount {
				return errors.New("Internal error, there are more local channels than expected")
			}
			curChan := ChannelAndAvailability{Channel: channelList[i].Channel, Avail: BasicAvailability{}}
			curChan.Avail.Availability = make([]AvailabilitySpan, 0, len(channelList[i].Avail.Availability))
			for _, avail := range channelList[i].Avail.Availability {
				if len(lookup.nameByIndex[avail.FrameType].FrameLabel) == 0 {
					curChan.Avail.Availability = append(curChan.Avail.Availability, avail)
				}
			}
			if err := encoder.Encode(&curChan); err != nil {
				return err
			}
			channelCount++
		}
		if channelCount != db.localchannelCount {
			return errors.New("Internal error, there are less local channels than expected")
		}
	}
	for i, _ := range lookup.frames {
		frameType := lookup.nameByIndex[i]
		if frameType.FrameLabel != "" {
			continue
		}
		bounds := common.GpsSecond(0)
		if b, ok := summaryRequest.FrameState[frameType.FrameName]; ok {
			bounds = b
		}
		frameExistance := lookup.frames[i].Read()
		jsonEntry := make([]JsonDBFrameInfo, 0, len(frameExistance))
		for _, span := range frameExistance {
			if span.End() <= bounds {
				continue
			}
			if span.Contains(bounds) {
				span = CreateTimeSpan(bounds, span.End())
			}
			jsonEntry = append(jsonEntry, JsonDBFrameInfo{
				FrameType: frameType.FrameName,
				TimeSpan:  span,
			})
		}
		if err := encoder.Encode(&jsonEntry); err != nil {
			return err
		}
	}
	return nil
}

func readReplicationStream(reader io.Reader) (*InternalJSONDB, error) {
	decoder := json.NewDecoder(reader)

	header := ReplicationHeader{}
	if err := decoder.Decode(&header); err != nil {
		return nil, err
	}
	db := InternalJSONDB{
		Channels:    make([]ChannelAndAvailability, header.NumChannels),
		FrameTypes:  make(map[string]int, len(header.FrameTypes)),
		Frames:      make([][]JsonDBFrameInfo, len(header.FrameTypes)),
		FrameSource: header.FrameSource,
	}
	for i := 0; i < header.NumChannels; i++ {
		if err := decoder.Decode(&db.Channels[i]); err != nil {
			return nil, err
		}
	}
	for i, frameType := range header.FrameTypes {
		frameInfo := make([]JsonDBFrameInfo, 0, 10)
		err := decoder.Decode(&frameInfo)
		if err != nil {
			return nil, err
		}
		db.FrameTypes[frameType] = i
		db.Frames[i] = frameInfo
	}
	return &db, nil
}

func channelLessByName(ch1, ch2 *BasicChannel) bool {
	if ch1.Name != ch2.Name {
		return ch1.Name < ch2.Name
	}
	if ch1.Class.Type < ch2.Class.Type {
		return true
	}
	if ch1.Class.Type > ch2.Class.Type {
		return false
	}
	if ch1.Rate < ch2.Rate {
		return true
	}
	if ch1.Rate > ch2.Rate {
		return false
	}
	if ch1.Type.Type < ch2.Type.Type {
		return true
	}
	if ch1.Type.Type > ch2.Type.Type {
		return false
	}
	return false
}

type sortChannelListByName struct {
	list []ChannelAndAvailability
}

func (s *sortChannelListByName) Len() int {
	return len(s.list)
}
func (s *sortChannelListByName) Less(i, j int) bool {
	return channelLessByName(&s.list[i].Channel, &s.list[j].Channel)
}

func (s *sortChannelListByName) Swap(i, j int) {
	tmp := s.list[i]
	s.list[i] = s.list[j]
	s.list[j] = tmp
}

func createMetaDataStoreFromJsonDB(internal *InternalJSONDB) (*MetaDataStore, error) {
	framesByName := make(map[string]int, len(internal.Frames))
	frames := make([]*TransactionalSpan, 0, len(internal.Frames))
	for key, index := range internal.FrameTypes {
		framesByName[key] = index
		timeSpans := make([]common.TimeSpan, 0, 10)

		for _, curFrameInfo := range internal.Frames[index] {
			timeSpans = append(timeSpans, curFrameInfo.TimeSpan.ToCommon())
		}
		frames = append(frames, CreateTransactionalSpan(timeSpans))
	}

	db := make([]ChannelAndAvailability, 0, len(internal.Channels))
	for _, channel := range internal.Channels {
		db = append(db, ChannelAndAvailability{Channel: channel.Channel,
			Avail: channel.Avail,
			Local: true,
		})
	}
	frameServer, err := common.GetFrameServer(internal.FrameSource)
	if err != nil {
		return nil, err
	}
	sort.Sort(&sortChannelListByName{db})
	mds := &MetaDataStore{db: db,
		frames: FrameView{
			frameByName: framesByName,
			nameByIndex: createNameByIndex(framesByName),
			frames:      frames,
		},
		frameServer:       frameServer,
		localchannelCount: len(db),
	}
	mds.localChannelChecksum = mds.calculateLocalChecksum()
	return mds, nil
}

// Add a remote server to data store.
// Remote servers have their channel lists combined with the local server
func (db *MetaDataStore) AddRemote(remote RemoteDataStore) error {
	db.updateLock.Lock()
	db.updateLock.Unlock()
	if remoteInterface, err := GetRemoteInterface(remote.ConnString); err != nil {
		return err
	} else {
		db.remoteServers = append(db.remoteServers, remoteDataStoreState{Remote: remote, RemoteInterface: remoteInterface})
		return nil
	}
}

// Given a frame label return a map of frame names (w/o the label) to index that match that label
func (fv *FrameView) getFramesWithLabel(label string) map[string]int {
	mapping := make(map[string]int)
	for i, frameType := range fv.nameByIndex {
		if frameType.FrameLabel == label {
			mapping[frameType.FrameName] = i
		}
	}
	return mapping
}

// Given a local and a remote channel list merge the two together
//
// This code handles updating the local channel list with a remote channel list.
// This handles:
//  * Channels that used to be on the remote server (as defined in having an availability span with a frame id in
//    oldRemoteFrameTyes) that is not currently in this remote list.  The availability spans are pruned, and if there
//    are no other availability spans, it is not included in the final list.
//  * Channels that are in the remote but not present in the local
//  * Channels that are in both lists, it will merge the availabilities together.
//
// As this is an update function, it may need to prune old entries that are no longer valid.  Frame types that should
// be removed from the system are listed in the oldRemoteFrameTypes list.
func rebuildLocalChannelListWithRemote(localChannels, remoteChannels []ChannelAndAvailability, oldRemoteFrameTypes []int) []ChannelAndAvailability {
	sort.Sort(&sortChannelListByName{remoteChannels})

	channelDest := make([]ChannelAndAvailability, len(remoteChannels)+len(localChannels))
	destIndex := 0
	localIndex := 0
	remoteIndex := 0
	localEnd := len(localChannels)
	remoteEnd := len(remoteChannels)
	for localIndex != localEnd && remoteIndex != remoteEnd {
		if channelLessByName(&localChannels[localIndex].Channel, &remoteChannels[remoteIndex].Channel) {
			removedCount := localChannels[localIndex].Avail.countBannedEntries(oldRemoteFrameTypes)
			if removedCount == len(localChannels[localIndex].Avail.Availability) {
				// this was a channel that used to be in the remote server but was dropped
				localIndex++
			} else if removedCount == 0 {
				channelDest[destIndex] = localChannels[localIndex]
				localIndex++
				destIndex++
			} else {
				// this was a channel that used to be present in the local + remote and was dropped from the remote
				newAvail := make([]AvailabilitySpan, 0, len(localChannels[localIndex].Avail.Availability)-removedCount)
				channelDest[destIndex] = localChannels[localIndex]
				channelDest[destIndex].Avail.Availability = localChannels[localIndex].Avail.removeFrameTypes(oldRemoteFrameTypes, newAvail)
				channelDest[destIndex].Avail.recalculateExistance()
				localIndex++
				destIndex++
			}
		} else if localChannels[localIndex].Channel == remoteChannels[remoteIndex].Channel {
			removedCount := localChannels[localIndex].Avail.countBannedEntries(oldRemoteFrameTypes)
			if removedCount == len(localChannels[localIndex].Avail.Availability) {
				// this was a channel that is was in the local + remote server but had no other local links
				channelDest[destIndex] = remoteChannels[remoteIndex]
			} else if removedCount == 0 {
				// this was in the local only previously and is now in local + remote
				newAvail := make([]AvailabilitySpan, 0, len(localChannels[localIndex].Avail.Availability)+len(remoteChannels[remoteIndex].Avail.Availability))
				newAvail = append(newAvail, localChannels[localIndex].Avail.Availability...)
				newAvail = append(newAvail, remoteChannels[remoteIndex].Avail.Availability...)
				channelDest[destIndex] = localChannels[localIndex]
				channelDest[destIndex].Avail.Availability = newAvail
				channelDest[destIndex].Avail.recalculateExistance()
			} else {
				// this was a channel that was in both local + remote and had mixed sources
				newAvail := make([]AvailabilitySpan, 0, len(localChannels[localIndex].Avail.Availability)-removedCount+len(remoteChannels[remoteIndex].Avail.Availability))
				newAvail = localChannels[localIndex].Avail.removeFrameTypes(oldRemoteFrameTypes, newAvail)
				newAvail = append(newAvail, remoteChannels[remoteIndex].Avail.Availability...)
				channelDest[destIndex] = localChannels[localIndex]
				channelDest[destIndex].Avail.Availability = newAvail
				channelDest[destIndex].Avail.recalculateExistance()
			}
			localIndex++
			remoteIndex++
			destIndex++
		} else {
			channelDest[destIndex] = remoteChannels[remoteIndex]
			remoteIndex++
			destIndex++
		}
	}
	for localIndex < localEnd {
		removedCount := localChannels[localIndex].Avail.countBannedEntries(oldRemoteFrameTypes)
		if removedCount == len(localChannels[localIndex].Avail.Availability) {
			// this was a channel that used to be in the remote server but was dropped
		} else if removedCount == 0 {
			channelDest[destIndex] = localChannels[localIndex]
			destIndex++
		} else {
			// this was a channel that used to be present in the local + remote and was dropped from the remote
			newAvail := make([]AvailabilitySpan, 0, len(localChannels[localIndex].Avail.Availability)-removedCount)
			channelDest[destIndex] = localChannels[localIndex]
			channelDest[destIndex].Avail.Availability = localChannels[localIndex].Avail.removeFrameTypes(oldRemoteFrameTypes, newAvail)
			channelDest[destIndex].Avail.recalculateExistance()
			destIndex++
		}
		localIndex++
	}
	for remoteIndex < remoteEnd {
		channelDest[destIndex] = remoteChannels[remoteIndex]
		remoteIndex++
		destIndex++
	}
	channelDest = channelDest[0:destIndex]
	return channelDest
}

// Update the channel and frame lists with data from the remote data source
func (db *MetaDataStore) updateFromRemoteServer(remote *remoteDataStoreState) {
	var channelDest []ChannelAndAvailability

	frameView := db.FrameView().Copy()
	frameToIndex := frameView.getFramesWithLabel(remote.Remote.Label)
	oldRemoteFrameTypes := make([]int, 0, len(frameToIndex))
	for _, frameId := range frameToIndex {
		oldRemoteFrameTypes = append(oldRemoteFrameTypes, frameId)
	}
	curFrameState := make(map[string]common.GpsSecond, len(frameToIndex))
	for frameType, frameIndex := range frameToIndex {
		existence := frameView.FrameExistance(frameIndex)
		if len(existence) > 0 {
			curFrameState[frameType] = existence[len(existence)-1].End()
		}
	}

	stream, err := remote.RemoteInterface.ReplicationStream(SummaryRequest{
		ChannelConfigHash: remote.ChannelConfigHash,
		FrameState:        curFrameState,
	})
	if err != nil {
		log.Printf("Unable to get replication stream from %s, %v", remote.Remote.Label, err)
		return
	}
	defer stream.Close()
	decoder := json.NewDecoder(stream)

	header := ReplicationHeader{}
	if err := decoder.Decode(&header); err != nil {
		log.Printf("Unable to deserialize the replication header from %s, %v", remote.Remote.Label, err)
		return
	}

	frameMapping := make([]int, len(header.FrameTypes))
	newFrames := make([]FrameType, 0, 10)
	for remoteIndex, frameType := range header.FrameTypes {
		var localIndex int
		var ok bool
		if localIndex, ok = frameToIndex[frameType]; !ok {
			localIndex = len(frameView.frames) + len(newFrames)
			frameToIndex[frameType] = localIndex
			newFrames = append(newFrames, NewFrameType(remote.Remote.Label, frameType))
		}
		frameMapping[remoteIndex] = localIndex
	}

	if !header.SkippedChannels {
		remoteChannels := make([]ChannelAndAvailability, header.NumChannels)
		for i := 0; i < header.NumChannels; i++ {
			if err := decoder.Decode(&remoteChannels[i]); err != nil {
				log.Printf("Unable to deserialze a channel in a replication stream, %v", err)
				return
			}
			remoteChannels[i].Avail.renumberFrames(frameMapping)
			remoteChannels[i].Avail.removeInvalid()
		}
		channelDest = rebuildLocalChannelListWithRemote(db.GetChannelView("").db, remoteChannels, oldRemoteFrameTypes)
	}

	for _, val := range newFrames {
		frameView.frameByName[val.PrintName] = len(frameView.frames)
		frameView.nameByIndex = append(frameView.nameByIndex, val)
		frameView.frames = append(frameView.frames, CreateTransactionalSpan([]common.TimeSpan{}))
	}

	for i := 0; i < len(header.FrameTypes); i++ {
		var frameInfo []JsonDBFrameInfo
		if err := decoder.Decode(&frameInfo); err != nil {
			return
		}
		frameIndex := frameView.frameByName[labeledFrameType(remote.Remote.Label, header.FrameTypes[i])]
		frameView.frames[frameIndex].Write(func(existence FrameExistence) FrameExistence {
			for _, entry := range frameInfo {
				curStart := entry.TimeSpan.Start()
				if len(existence) > 0 && existence[len(existence)-1].End() == curStart {
					existence[len(existence)-1].Extend(entry.TimeSpan.End())
				} else {
					existence = append(existence, entry.TimeSpan)
				}
			}
			return existence
		})
	}

	db.setFrameLookup(&frameView)

	// frame updated must be installed prior to channel updates
	if channelDest != nil {
		db.channelLock.Lock()
		db.db = channelDest
		db.channelLock.Unlock()
	}
}

// Query each registered remote server for updates and modify the
// channel/frame list as needed.
//
// Currently errors stop progress
//
// Note this process may take signifigant time and generate a fair
// amount of garbage for the GC.
//
// This can (and should) be called periodically to keep the lists
// updated.
func (db *MetaDataStore) UpdateRemotes() error {
	db.updateLock.Lock()
	db.updateLock.Unlock()
	for i, _ := range db.remoteServers {
		db.updateFromRemoteServer(&db.remoteServers[i])
	}
	return nil
}

// Calculate a checksum off the channel configuration for all
// local channels.
//
// Note this only calculates the checksum, it does not store it
// on the MetaDataStore
func (db *MetaDataStore) calculateLocalChecksum() []byte {
	lookup := db.FrameView()
	h := sha512.New512_256()
	for i, _ := range db.db {
		db.db[i].applyToLocalHash(h, lookup.nameByIndex)
	}
	results := []byte{}
	return h.Sum(results)
}

// Create a MetaDataStore from a FrameTypeRegistrar
func createMetaDataStore(fr *FrameTypeRegistrar, frameServer common.FrameServer) *MetaDataStore {
	db := make([]ChannelAndAvailability, 0, len(fr.db))
	for coreChannel, coreAvail := range fr.db {
		db = append(db, ChannelAndAvailability{
			Channel: coreChannel,
			Avail:   coreAvail,
			Local:   true,
		})
	}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		sort.Sort(&sortChannelListByName{db})
	}()

	for frameType, index := range fr.frameByName {
		log.Printf("Retreiving frame availability for '%s'", frameType)
		frameAvail, err := frameServer.FrameIntervals(frameType, common.CreateTimeSpan(0, common.GpsInf))
		if err != nil {
			log.Printf("Error retreiving frame availability for '%s' %v", frameType, err)
		} else {
			log.Printf("Retreived %d entries for '%s'", len(frameAvail), frameType)
			fr.frames[index] = frameAvail
		}
	}
	wg.Wait()

	frameSpans := 0
	for _, v := range fr.frames {
		frameSpans += len(v)
	}
	log.Printf("MetaDataStore created with %d channels, %d frame types, %d frame spans",
		len(db),
		len(fr.frames),
		frameSpans)

	frames := make([]*TransactionalSpan, 0, len(fr.frames))
	for _, entry := range fr.frames {
		frames = append(frames, CreateTransactionalSpan(entry))
	}

	mds := &MetaDataStore{
		db: db,
		frames: FrameView{
			frameByName: fr.frameByName,
			nameByIndex: createNameByIndex(fr.frameByName),
			frames:      frames,
		},
		frameServer: frameServer,
	}
	mds.localchannelCount = len(mds.db)
	mds.localChannelChecksum = mds.calculateLocalChecksum()
	return mds
}

func (m *MetaDataStore) saveJsonDb(jsonPath string) error {
	f, err := os.Create(jsonPath)
	if err != nil {
		return err
	}
	defer f.Close()
	var writer io.Writer
	writer = f
	if strings.HasSuffix(jsonPath, ".gz") {
		gzWriter := gzip.NewWriter(f)
		defer gzWriter.Close()
		writer = gzWriter
	}
	return m.ReplicationStream(writer, SummaryRequest{})
}
