//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/cmux"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/diskcache"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

import (
	"git.ligo.org/nds/nds-metadata-server/mds"
	"git.ligo.org/nds/nds-metadata-server/nds2parser"
	"git.ligo.org/nds/nds-metadata-server/server"
)

type FrameUpdateConfig struct {
	FrameName string
	Period    time.Duration
}

type NDSConfig struct {
	ListenAddress     string
	FramesAdress      string
	ChannelListFiles  []string
	ReplicationServer string
	RemoteServers     []string
	JSonDB            string
	FrameDir          string
	Epochs            server.Epoch
	DiskCacheServer   string
	LocalReader       string
	Updaters          []FrameUpdateConfig
}

type ExternalDiskCache struct {
	endpoint string
	cache    *diskcache.ExternalDiskCacheServer
}

func NewExternalDiskCache(endpoint string) *ExternalDiskCache {
	return &ExternalDiskCache{endpoint: endpoint, cache: nil}
}

func (dc *ExternalDiskCache) Get() *diskcache.ExternalDiskCacheServer {
	if dc.cache == nil {
		dc.cache = diskcache.NewExternalDiskCacheServer(dc.endpoint, 6)
	}
	return dc.cache
}
func (dc *ExternalDiskCache) Close() error {
	if dc.cache != nil {
		dc.cache.Close()
		dc.cache = nil
	}
	return nil
}

func httpProtoclMatcher(data []byte) bool {
	methods := []string{"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH", "UPGRADE"}
	input := string(data)
	for _, method := range methods {
		l := len(method)
		if len(input) < l {
			l = len(input)
		}
		if input[0:l] == method[0:l] {
			return true
		}
	}
	return false
}

func handleConnection(conn net.Conn, db *mds.MetaDataStore, epochs server.Epoch) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Client handler paniced.", r)
		}
	}()
	defer conn.Close()
	bufferedConn := bufio.NewWriter(conn)
	defer bufferedConn.Flush()

	client := server.CreateClient(bufferedConn, db, epochs)

	reader := bufio.NewScanner(conn)
	for reader.Scan() {
		fmt.Printf("'%s'\n", reader.Text())
		cmd := nds2parser.ParseCommand(reader.Text())
		if err := client.Process(cmd); err != nil {
			break
		}
	}
}

func parseUpdaterList(input string) ([]FrameUpdateConfig, error) {
	updaters := make([]FrameUpdateConfig, 0)
	if input != "" {
		for _, entry := range strings.Split(input, ",") {
			mid := strings.Index(entry, ":")
			if mid <= 0 {
				return nil, errors.New("invalid updater specification")
			}
			frameName := entry[0:mid]
			if strings.HasPrefix(frameName, "(") {
				return nil, errors.New("updaters are not valid for remote frame types")
			}
			count, err := strconv.ParseUint(entry[mid+1:], 10, 32)
			if err != nil {
				return nil, errors.New("invalid period specification in updater")
			}
			if count == 0 || count > 3600*4 {
				return nil, errors.New("invalid period range")
			}
			updaters = append(updaters, FrameUpdateConfig{FrameName: frameName, Period: time.Second * time.Duration(count)})
		}
	}
	return updaters, nil
}

func ParseArgs() NDSConfig {
	jsonDb := flag.String("json-db", "", "If set read the channel db from the json file specified")
	channelLists := flag.String("channel-lists", "", "File containing the paths of the channel lists to load")
	frameDir := flag.String("frame-dir", "", "Directory of frames to serve from")
	replicationServer := flag.String("replicate-from", "", "Set to the hostname:port of a primary replication server")
	epochList := flag.String("epochs", "", "File containing the epoch definitions")
	listen := flag.String("listen", "localhost:31200", "Interface:port specification of the port to listen on (nds2 & 3)")
	diskCache := flag.String("diskcache", "localhost:11300", "Connection string for the disk cache server")
	remoteServers := flag.String("remote-servers", "", "A list of comma separated remote server connection strings")
	localReader := flag.String("local-reader", "", "The network address of a nds io node to allow this server to serve reads from remote servers")
	updaters := flag.String("frame-updaters", "", "A common seperated list of frametypes and update periods in seconds, ex A1:64,H1:64,L1:64")

	flag.Parse()
	methods := 0
	if len(*frameDir) > 0 {
		methods++
	}
	if len(*jsonDb) > 0 && len(*frameDir) == 0 {
		methods++
	}
	if len(*channelLists) > 0 {
		methods++
	}
	if len(*replicationServer) > 0 {
		methods++
	}
	if methods != 1 {
		log.Fatal("One (and only one) of json-db, channel-lists, frame-dir, or replicate-from must be specified")
	}
	remoteServerList := make([]string, 0)
	if *remoteServers != "" {
		remoteServerList = strings.Split(*remoteServers, ",")
	}
	if len(*replicationServer) != 0 && len(remoteServerList) != 0 {
		log.Fatal("Replication and remote servers are not allowed at the same time")
	}
	if *frameDir != "" && *jsonDb == "" {
		*jsonDb = "frame_index.json"
	}
	finalLists := make([]string, 0, 10)
	if len(*channelLists) != 0 {
		f, err := os.Open(*channelLists)
		if err != nil {
			log.Fatalf("Unable to open channel lists file '%s', error %v\n", *channelLists, err)
		}
		defer f.Close()
		reader := bufio.NewScanner(f)
		for reader.Scan() {
			line := strings.Trim(reader.Text(), " \t")
			if line == "" || strings.HasPrefix(line, "#") {
				continue
			}
			finalLists = append(finalLists, line)
		}
	}
	requestedUpdaters, err := parseUpdaterList(*updaters)
	if err != nil {
		log.Fatal("Invalid updater list")
	}
	return NDSConfig{
		ListenAddress:     *listen,
		ChannelListFiles:  finalLists,
		FrameDir:          *frameDir,
		ReplicationServer: *replicationServer,
		JSonDB:            *jsonDb,
		Epochs:            server.LoadEpochs(*epochList),
		DiskCacheServer:   *diskCache,
		RemoteServers:     remoteServerList,
		LocalReader:       *localReader,
		Updaters:          requestedUpdaters,
	}
}

func loadFromReplicationStream(serverLocation string) (*mds.MetaDataStore, error) {
	url := "http://" + serverLocation + "/replicate/"
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error acquiring replication stream at '%s', %v", url, err)
		return nil, err
	}
	defer resp.Body.Close()
	mds, err := mds.LoadDatabaseFromReplicationStream(resp.Body)
	if err != nil {
		log.Printf("Unable to load replication stream, %v", err)
		return nil, err
	}
	return mds, nil
}

func runRemoteUpdaters(mdb *mds.MetaDataStore, remoteServers []string) {
	if len(remoteServers) < 1 {
		return
	}
	for _, entry := range remoteServers {
		parts := strings.SplitN(entry, ":", 2)
		if len(parts) != 2 {
			log.Fatalf("Malformed remote string, must be label ':' connection string")
		}
		err := mdb.AddRemote(mds.RemoteDataStore{Label: parts[0], ConnString: parts[1]})
		if err != nil {
			log.Fatalf("Unable to create remote interface for %s, %v", entry, err)
		}
	}
	for {
		mdb.UpdateRemotes()
		time.Sleep(time.Minute * 5)
	}
}

func runFrameUpdater(mdb *mds.MetaDataStore, config FrameUpdateConfig, cacheServer *diskcache.ExternalDiskCacheServer) {
	view := mdb.FrameView()
	index := view.FrameIndex(config.FrameName)
	if index < 0 {
		log.Printf("Unknown frame type, '%s' updater aborting", config.FrameName)
		return
	}
	for {
		time.Sleep(config.Period)
		view = mdb.FrameView()
		if trans, _, ok := view.GetTransactionalSpan(index); ok {
			spans := trans.Read()
			start := mds.GpsSecond(0)
			if len(spans) > 0 {
				start = spans[len(spans)-1].End()
			}
			newSpans, err := cacheServer.FrameIntervals(config.FrameName, common.CreateTimeSpan(start, common.GpsInf))
			if err == nil && len(newSpans) > 0 {
				trans.Write(func(existence mds.FrameExistence) mds.FrameExistence {
					empty := len(existence) == 0
					for _, curSpan := range newSpans {
						if !empty {
							if existence[len(existence)-1].End() == curSpan.Start() {
								existence[len(existence)-1].Extend(curSpan.End())
								continue
							}
						}
						existence = append(existence, mds.CreateTimeSpan(curSpan.Start(), curSpan.End()))
					}
					return existence
				})
			}

		}
	}
}

func main() {
	config := ParseArgs()
	fmt.Println("Hello nds")

	var mdb *mds.MetaDataStore
	var err error
	frameServer := NewExternalDiskCache(config.DiskCacheServer)
	defer frameServer.Close()

	if config.FrameDir != "" {
		mdb, err = mds.LoadDatabaseFromFrames(config.FrameDir, config.JSonDB)
		if err != nil {
			log.Fatalf("Unable to load frames, %v", err)
		}
	} else if config.JSonDB != "" {
		mdb, err = mds.LoadDatabaseFromJsonDB(config.JSonDB)
		if err != nil {
			log.Fatalf("Unable to load json database, %v", err)
		}
	} else if config.ReplicationServer != "" {
		mdb, err = loadFromReplicationStream(config.ReplicationServer)
		if err != nil {
			log.Fatalf("Unable to load replication stream, %v", err)
		}
	} else {
		mdb = mds.LoadDatabaseFromChannelLists(config.ChannelListFiles, frameServer.Get())
	}
	mdb.DebugDump(os.Stdout, 10)

	if len(config.RemoteServers) > 0 {
		fmt.Printf("remote servers = '%v'\n", config.RemoteServers)
		go runRemoteUpdaters(mdb, config.RemoteServers)
	}

	for _, updaterSpec := range config.Updaters {
		go runFrameUpdater(mdb, updaterSpec, frameServer.Get())
	}

	listener, err := net.Listen("tcp", config.ListenAddress)
	if err != nil {
		log.Fatalf("Unable to open listening socket at %s, %v", config.ListenAddress, err)
	}
	muxListener := cmux.NewCMux(listener)
	httpListener, err := muxListener.RegisterProtocol(httpProtoclMatcher)
	if err != nil {
		log.Fatalf("Unable to create http listener, %v", err)
	}
	go server.ServeNds3(httpListener, func() *mds.MetaDataStore {
		return mdb
	}, config.LocalReader)

	nds2Listener, err := muxListener.RegisterProtocol(cmux.MatchAll)
	if err != nil {
		log.Fatalf("Unable to create nds2 listener, %v", err)
	}

	muxListener.Start()
	defer muxListener.Close()

	fmt.Println("NDS meta data server listening on ", config.ListenAddress)
	for {
		conn, err := nds2Listener.Accept()
		if err != nil {
			continue
		}
		go handleConnection(conn,
			mdb, config.Epochs)
	}
}
