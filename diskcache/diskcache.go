//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package diskcache

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
)

import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

const (
	dcIntervals = iota
	dcPaths
)

type diskCacheCommand struct {
	Cmd            int
	FrameType      string
	TimeSpan       common.TimeSpan
	IntervalResult chan []common.TimeSpan
	PathResult     chan []string
}

func formatIntervalsCommand(frameType string, span common.TimeSpan) string {
	cmd := fmt.Sprintf("intervals --extension .gwf --ifo-type-list %s --start-time %d --end-time %d",
		frameType, span.Start(), span.End())
	return strconv.Itoa(len(cmd)) + cmd
}

func formatPathCommand(frameType string, span common.TimeSpan) string {
	cmd := fmt.Sprintf("filenames --extension .gwf --ifo-type-list %s --start-time %d --end-time %d",
		frameType, span.Start(), span.End())
	return strconv.Itoa(len(cmd)) + cmd
}

func parseFrameAvailability(input string) []common.TimeSpan {
	spans := make([]common.TimeSpan, 0, 10)
	scanner := bufio.NewScanner(strings.NewReader(input))
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), "[) ")
		parts := strings.SplitN(line, ", ", 2)
		if len(parts) != 2 {
			continue
		}
		start, err_start := strconv.ParseInt(parts[0], 10, 64)
		end, err_end := strconv.ParseInt(parts[1], 10, 64)
		if err_start != nil || err_end != nil {
			continue
		}
		spans = append(spans, common.CreateTimeSpan(start, end))
	}
	return spans
}

func parseFramePaths(input string) (results []string) {
	results = make([]string, 0, 10)
	for _, val := range strings.Split(input, "\n") {
		if val != "" {
			results = append(results, val)
		}
	}
	return
}

func FrameAvailability(address string, frameType string, span common.TimeSpan) ([]common.TimeSpan, error) {
	spans := make([]common.TimeSpan, 0, 0)
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return spans, err
	}
	defer conn.Close()

	cmd := formatIntervalsCommand(frameType, span)
	if _, err = conn.Write([]byte(cmd)); err != nil {
		return spans, err
	}
	var haveData byte
	if err = binary.Read(conn, binary.LittleEndian, &haveData); err != nil {
		return spans, err
	}
	if haveData == 0 {
		return spans, nil
	}
	remaining := int32(0)
	if err = binary.Read(conn, binary.LittleEndian, &remaining); err != nil {
		return spans, err
	}
	totalRequired := remaining
	line := make([]byte, totalRequired, totalRequired)
	received := 0
	for remaining > 0 {

		count, err := conn.Read(line[received:totalRequired])
		remaining -= int32(count)
		received += count
		if err != nil && remaining > 0 {
			return spans, err
		}
	}
	return parseFrameAvailability(string(line)), nil
}

func FramePaths(address string, frameType string, span common.TimeSpan) ([]string, error) {
	paths := make([]string, 0, 0)
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return paths, err
	}
	defer conn.Close()

	cmd := formatPathCommand(frameType, span)
	if _, err = conn.Write([]byte(cmd)); err != nil {
		return paths, err
	}
	var haveData byte
	if err = binary.Read(conn, binary.LittleEndian, &haveData); err != nil {
		return paths, err
	}
	if haveData == 0 {
		return paths, nil
	}
	remaining := int32(0)
	if err = binary.Read(conn, binary.LittleEndian, &remaining); err != nil {
		return paths, err
	}
	totalRequired := remaining
	line := make([]byte, totalRequired, totalRequired)
	received := 0
	for remaining > 0 {

		count, err := conn.Read(line[received:totalRequired])
		remaining -= int32(count)
		received += count
		if err != nil && remaining > 0 {
			return paths, err
		}
	}
	return parseFramePaths(string(line)), nil
}

type ExternalDiskCacheServer struct {
	address  string
	poolSize int
	pool     chan diskCacheCommand
	shutdown *sync.WaitGroup
}

func diskCachePool(address string, ch chan diskCacheCommand, group *sync.WaitGroup) {
	defer group.Done()
	for cmd := range ch {
		switch cmd.Cmd {
		case dcIntervals:
			if cmd.IntervalResult == nil {
				continue
			}
			if spans, err := FrameAvailability(address, cmd.FrameType, cmd.TimeSpan); err != nil {
				close(cmd.IntervalResult)
			} else {
				cmd.IntervalResult <- spans
			}
		case dcPaths:
			if cmd.PathResult == nil {
				continue
			}
			if paths, err := FramePaths(address, cmd.FrameType, cmd.TimeSpan); err != nil {
				close(cmd.IntervalResult)
			} else {
				cmd.PathResult <- paths
			}
		}
	}
}

func NewExternalDiskCacheServer(address string, poolSize int) *ExternalDiskCacheServer {
	if poolSize < 1 {
		poolSize = 1
	}
	pool := make(chan diskCacheCommand)
	shutdown := &sync.WaitGroup{}
	for i := 0; i < poolSize; i++ {
		shutdown.Add(1)
		go diskCachePool(address, pool, shutdown)
	}
	return &ExternalDiskCacheServer{address: address, poolSize: poolSize, pool: pool, shutdown: shutdown}
}

func (dc *ExternalDiskCacheServer) FrameIntervals(frameType string, timeSpan common.TimeSpan) ([]common.TimeSpan, error) {
	resultChan := make(chan []common.TimeSpan)
	dc.pool <- diskCacheCommand{
		Cmd:            dcIntervals,
		FrameType:      frameType,
		TimeSpan:       timeSpan,
		IntervalResult: resultChan,
		PathResult:     nil,
	}
	if results, ok := <-resultChan; ok {
		return results, nil
	} else {
		return nil, errors.New("Unable to get frame intervals")
	}
}

func (dc *ExternalDiskCacheServer) FrameFilePaths(frameType string, timeSpan common.TimeSpan) ([]string, error) {
	resultChan := make(chan []string)
	dc.pool <- diskCacheCommand{
		Cmd:            dcPaths,
		FrameType:      frameType,
		TimeSpan:       timeSpan,
		IntervalResult: nil,
		PathResult:     resultChan,
	}
	if results, ok := <-resultChan; ok {
		return results, nil
	} else {
		return nil, errors.New("Unable to get frame paths")
	}
}

func (dc *ExternalDiskCacheServer) ConnectionString() string {
	return "disk_cache://" + dc.address
}

func (dc *ExternalDiskCacheServer) Close() {
	close(dc.pool)
	dc.shutdown.Wait()
}

func init() {
	common.RegisterFrameServer("disk_cache", func(address string) (common.FrameServer, error) {
		return NewExternalDiskCacheServer(address, 4), nil
	})
}
